# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from version import __version__

import os

import flask
# from flask.ext.socketio import SocketIO
from flask_socketio import SocketIO     #    !!!!!!!!!!!!!!!!!!!

# from gevent import monkey        #    !!!!!!!!!!!!!!!!!!!!
# monkey.patch_all()

import config
from config import config_value  # noqa

import utils  # noqa
from utils import filesystem as fs  # noqa
from utils.store import StoreCache  # noqa

import scheduler  # noqa        #    !!!!!!!!!!!!!!!!!!!!!!

# Create Flask, Scheduler and SocketIO objects

url_prefix = config_value('url_prefix')
app = flask.Flask(__name__, static_url_path=url_prefix+'/static')
app.config['DEBUG'] = True
# Disable CSRF checking in WTForms
app.config['WTF_CSRF_ENABLED'] = False
# This is still necessary for SocketIO
# app.config['SECRET_KEY'] = os.urandom(12).encode('hex')        #    !!!!!!!!!!!!!!!!!
app.url_map.redirect_defaults = False
app.config['URL_PREFIX'] = url_prefix
# socketio = SocketIO(app, async_mode='gevent', path=url_prefix+'/socket.io')
socketio = SocketIO(app, path=url_prefix+'/socket.io')      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
app.config['store_cache'] = StoreCache()
app.config['store_url_list'] = config_value('model_store')['url_list']
scheduler = scheduler.Scheduler(config_value('gpu_list'), True)      #    !!!!!!!!!!!!!!!

# Register filters and views
app.jinja_env.globals['server_name'] = config_value('server_name')
app.jinja_env.globals['server_version'] = __version__

# app.jinja_env.globals['dir_hash'] = fs.dir_hash(os.path.join(os.path.dirname(__file__), 'static'))    #    !!!!!!!!!!!!!!!!!
app.jinja_env.filters['print_time'] = utils.time_filters.print_time
app.jinja_env.filters['print_time_diff'] = utils.time_filters.print_time_diff
app.jinja_env.filters['print_time_since'] = utils.time_filters.print_time_since
app.jinja_env.filters['sizeof_fmt'] = utils.sizeof_fmt
app.jinja_env.filters['has_permission'] = utils.auth.has_permission
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

import views  # noqa
app.register_blueprint(views.blueprint,url_prefix=url_prefix)
import dataset.views  # noqa
app.register_blueprint(dataset.views.blueprint,url_prefix=url_prefix+'/datasets')
import dataset.generic.views  # noqa
app.register_blueprint(dataset.generic.views.blueprint,url_prefix=url_prefix+'/datasets/generic')
# import dataset.texts.views  # noqa
# app.register_blueprint(dataset.texts.views.blueprint,
#                        url_prefix=url_prefix+'/datasets/texts')
# import dataset.texts.classification.views  # noqa
# app.register_blueprint(dataset.texts.classification.views.blueprint,
#                        url_prefix=url_prefix+'/datasets/texts/classification')
# import dataset.texts.generic.views  # noqa
# app.register_blueprint(dataset.texts.generic.views.blueprint,
#                        url_prefix=url_prefix+'/datasets/texts/generic')
import model.views  # noqa
app.register_blueprint(model.views.blueprint,url_prefix=url_prefix+'/models')
import model.texts.views  # noqa
app.register_blueprint(model.texts.views.blueprint,url_prefix=url_prefix+'/models/texts')
import model.texts.classification.views  # noqa
app.register_blueprint(model.texts.classification.views.blueprint,url_prefix=url_prefix+'/models/texts/classification')
import model.texts.generic.views  # noqa
app.register_blueprint(model.texts.generic.views.blueprint,url_prefix=url_prefix+'/models/texts/generic')
# import pretrained_model.views  # noqa
# app.register_blueprint(pretrained_model.views.blueprint,url_prefix=url_prefix+'/pretrained_models')
# import store.views  # noqa
# app.register_blueprint(store.views.blueprint,url_prefix=url_prefix+'/store')

def username_decorator(f):
    from functools import wraps

    @wraps(f)
    def decorated(*args, **kwargs):
        this_username = flask.request.cookies.get('username', None)
        app.jinja_env.globals['username'] = this_username
        return f(*args, **kwargs)
    return decorated

# for endpoint, function in app.view_functions.iteritems():
for endpoint, function in app.view_functions.items():    
    
    app.view_functions[endpoint] = username_decorator(function)

# Setup the environment

scheduler.load_past_jobs()
