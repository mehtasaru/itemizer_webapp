#  Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#    Changed by LevaData 2019

#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
"""Example of Estimator for DNN-based text classification with DBpedia data."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys

import numpy as np
# import pandas
from sklearn import metrics
import tensorflow as tf

# MAX_DOCUMENT_LENGTH = 20     #   Started with 10     !!!!!!!!!!!!!!!!!
# EMBEDDING_SIZE = 100         #   Started with 50     !!!!!!!!!!!!!!!!!
# MAX_LABEL = 60               #  Started with 15 
         
WORDS_FEATURE = 'words'  # Name of the input words feature.
boolBAG_OF_WORDS=True  #    !!!!!!!!!!!!!!!!!!!!!!!!!!!

FLAGS = None
n_words = 0
n_classes=0
# n_classes=MAX_LABEL

def estimator_spec_for_softmax_classification(
    logits, labels, mode):
    """Returns EstimatorSpec instance for softmax classification."""

    global n_classes,n_words  #   !!!!!!!!!!!!!!!!!!!!

    predicted_classes = tf.argmax(logits, 1)
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions={
                'class': predicted_classes,
                'prob': tf.nn.softmax(logits)
            })

    onehot_labels = tf.one_hot(labels, n_classes, 1, 0)        #   !!!!!!!!!!!!!
    loss = tf.losses.softmax_cross_entropy(
      onehot_labels=onehot_labels, logits=logits)
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
        train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)

    eval_metric_ops = {
      'accuracy': tf.metrics.accuracy(
          labels=labels, predictions=predicted_classes)
    }
    return tf.estimator.EstimatorSpec(
      mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def bag_of_words_model(features, labels, mode):
    """A bag-of-words model. Note it disregards the word order in the text."""

    global n_classes,n_words,n_EMBEDDING_SIZE

    bow_column = tf.feature_column.categorical_column_with_identity(
      WORDS_FEATURE, num_buckets=n_words)
    bow_embedding_column = tf.feature_column.embedding_column(
      bow_column, dimension=n_EMBEDDING_SIZE)
    bow = tf.feature_column.input_layer(
      features,
      feature_columns=[bow_embedding_column])

    logits = tf.layers.dense(bow, n_classes, activation=None)

    return estimator_spec_for_softmax_classification(logits=logits, labels=labels, mode=mode)


def rnn_model(features, labels, mode):
    """RNN model to predict from sequence of words to a class."""
    # Convert indexes of words into embeddings.
    # This creates embeddings matrix of [n_words, EMBEDDING_SIZE] and then
    # maps word indexes of the sequence into [batch_size, sequence_length,
    # EMBEDDING_SIZE].

    global n_classes,n_words,n_EMBEDDING_SIZE

    word_vectors = tf.contrib.layers.embed_sequence(
      features[WORDS_FEATURE], vocab_size=n_words, embed_dim=n_EMBEDDING_SIZE)

    # Split into list of embedding per word, while removing doc length dim.
    # word_list results to be a list of tensors [batch_size, EMBEDDING_SIZE].
    word_list = tf.unstack(word_vectors, axis=1)

    # Create a Gated Recurrent Unit cell with hidden size of EMBEDDING_SIZE.
    cell = tf.contrib.rnn.GRUCell(n_EMBEDDING_SIZE)

    # Create an unrolled Recurrent Neural Networks to length of
    # MAX_DOCUMENT_LENGTH and passes word_list as inputs for each unit.
    #_, encoding = tf.contrib.rnn.static_rnn(cell, word_list, dtype=tf.float32)
    myOutput, encoding = tf.contrib.rnn.static_rnn(cell, word_list, dtype=tf.float32)

    # Given encoding of RNN, take encoding of last step (e.g hidden size of the
    # neural network of last step) and pass it as features for softmax
    # classification over output classes.
    #logits = tf.layers.dense(encoding, MAX_LABEL, activation=None)
    logits = tf.layers.dense(encoding, n_classes, activation=None)    #   !!!!!!!!!!!!!!!!

    return estimator_spec_for_softmax_classification(logits=logits, labels=labels, mode=mode)

#   ************ LevaData 'main' function *********************
def main(x_train,y_train,x_test,y_test,x_ver,lenDictClasslabels,intALGORITHM_COMBO_BOX_INDEX,classifier,vocab_processor,model_dir,intBatchSize,
         intMAX_DOCUMENT_LENGTH,intEMBEDDING_SIZE,intNUMBER_OF_STEPS):

    global n_words,n_classes,n_EMBEDDING_SIZE

    n_classes=lenDictClasslabels
    n_EMBEDDING_SIZE=intEMBEDDING_SIZE

#  *********** Training *************
    if x_ver is None:  

        # Process vocabulary
        vocab_processor = tf.contrib.learn.preprocessing.VocabularyProcessor(intMAX_DOCUMENT_LENGTH)
    
        x_transform_train = vocab_processor.fit_transform(x_train)
        x_transform_test = vocab_processor.transform(x_test)
    
        list_x_train=list(x_transform_train)
        list_x_test=list(x_transform_test)
    
        x_train = np.array(list_x_train)
        x_test = np.array(list_x_test)
    
        n_words = len(vocab_processor.vocabulary_)
#       print('Total words: %d' % n_words)
    
# ******** Build model **************
# ******** Switch between rnn_model and bag_of_words_model to test different models *********
        model_fn = rnn_model
    
        if intALGORITHM_COMBO_BOX_INDEX==0:
        
# Subtract 1 because VocabularyProcessor outputs a word-id matrix where word
# ids start from 1 and 0 means 'no word'. But
# categorical_column_with_identity assumes 0-based count and uses -1 for
# missing word.
            x_train -= 1
            x_test -= 1
        
            model_fn = bag_of_words_model
    
#  ********** Define estimator **********
        classifier = tf.estimator.Estimator(model_fn=model_fn,model_dir=model_dir)
    
# *********** Define classifier inputs for training *********
      
        if intBatchSize==0:
            train_input_fn = tf.estimator.inputs.numpy_input_fn(
                x={WORDS_FEATURE: x_train},
                y=y_train,
                batch_size=len(x_train),
                num_epochs=None,
                shuffle=True)
        else:
            train_input_fn = tf.estimator.inputs.numpy_input_fn(
                x={WORDS_FEATURE: x_train},
                y=y_train,
                batch_size=intBatchSize,
                #batch_size=128,
                num_epochs=None,
                #num_epochs=100,
                shuffle=True)        
    
# *********  Train classifier based on training data *********
#         classifier.train(input_fn=train_input_fn, steps=100)
        classifier.train(input_fn=train_input_fn, steps=intNUMBER_OF_STEPS)        
    
# ********* Define classifier inputs for testing ************
        test_input_fn = tf.estimator.inputs.numpy_input_fn(
          x={WORDS_FEATURE: x_test},
          num_epochs=1,
          shuffle=False)
    
# **********  Predict labels/classes using trained classifier on testing data *********
        predictions = classifier.predict(input_fn=test_input_fn)
    
# **********  Extract predictions list ************
        myList=list(p['class'] for p in predictions)
    
# ********** Convert predictions list into array *******
        y_predicted = np.array(myList)
    
# **********  Reshape predictions after 'y_test' array ********* 
        y_predicted = y_predicted.reshape(np.array(y_test).shape)
    
# ********** Score accuracy with sklearn ***********
        score = metrics.accuracy_score(y_test, y_predicted)
#     score = metrics.accuracy_score(y_test, y_predicted,normalize=False)
#     print('Accuracy: {0:f}'.format(score))
      
        y_predictedVer=None
  
        return classifier,vocab_processor,score,y_predictedVer

#  ********** Testing/validation/verification ***********
    else:
    
        n_words = len(vocab_processor.vocabulary_)

#  ******** Apply vocab_processor ***********
        x_transform_ver = vocab_processor.transform(x_ver)

#  ********* Set verification list ***********      
        list_x_ver=list(x_transform_ver)

# **********  Convert to verification array **********       
        x_ver = np.array(list_x_ver)

#  ******** Define model ************
        model_fn = rnn_model 
      
        if intALGORITHM_COMBO_BOX_INDEX==0:
        
        # Subtract 1 because VocabularyProcessor outputs a word-id matrix where word
        # ids start from 1 and 0 means 'no word'. But
        # categorical_column_with_identity assumes 0-based count and uses -1 for
        # missing word.
         
            x_ver -= 1      #   For verification as well
    
            model_fn = bag_of_words_model 
        
# ********* Define classifier inputs **********
        ver_input_fn = tf.estimator.inputs.numpy_input_fn(
          x={WORDS_FEATURE: x_ver},
          num_epochs=1,
          shuffle=False)
    
# *********  Predict labels/classes on verification data
        predictionsVer = classifier.predict(input_fn=ver_input_fn)

        #myListVer=list(p['class'] for p in predictionsVer)
# *********  Extract predictions list ***********
        myListVer = []
        probs_uncl = []
        for p in predictionsVer:
            myListVer.append(p['class'])
            probs_uncl.append(p['prob'])
    
# ********* Convert predictions list into array **********
        y_predictedVer = np.array(myListVer)
        probs = np.array(probs_uncl)

        score=None
      
        return classifier,vocab_processor,score,y_predictedVer,probs
 
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
      '--test_with_fake_data',
      default=False,
      help='Test the example code with fake data.',
      action='store_true')
    parser.add_argument(
      '--bow_model',
      default=False,
      help='Run with BOW model instead of RNN.',
      action='store_true')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
