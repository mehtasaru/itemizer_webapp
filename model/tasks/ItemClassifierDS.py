#  ********** Imports ****************
import sys
import os
import subprocess
import numpy as np
import pandas as pd
import re
import pickle
import os.path
import shutil
import distutils.util
import datetime
import copy
from . import text_classification_LD as tc
import configparser

#    ************* For Bayes *************************
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import RandomForestClassifier

#    ************** Constant lists *********************
listClassificationAlgorithms = ['Bag-of-words', 'Recurrent NN', 'Multinomial_NB']


#    ****************** ML processing class *******************
class MLSituation():
    def __init__(self):

        self.data = None  # dataframe
        self.trainingData = None
        self.testingData = None
        self.verifyData = None

        self.y = None  # The column to predict
        self.mapper = None
        self.dictionary = None

    def setDictionary(self):
        #         from sklearn.feature_extraction.text import CountVectorizer
        self.dictionary = sorted(list(CountVectorizer().fit(self.data["X"]).vocabulary_.keys()))

    def setMapper(self):
        #         from sklearn import preprocessing
        mlele = preprocessing.LabelEncoder()
        mlele.fit(list(self.data[self.y]))
        self.mapper = mlele
        return (None)

    def cleanupString(self, strDescription):

        if strDescription == '_NA':
            return strDescription

        strDescription = strDescription.upper()

        #  Discard leading special symbols
        lenStrDescr = len(strDescription)
        j = 0
        while not strDescription[j].isalpha() and not strDescription[
            j].isdigit() and j < lenStrDescr - 1:  # Discard first non-letters
            j += 1
        if j > 0:
            strDescription = strDescription[j:]

        lenStrDescr = len(strDescription)
        for j in range(lenStrDescr):
            if strDescription[j].isalpha():  # Continue till end of letters
                continue
            if strDescription[j] == ',':  # Replace first comma by space
                if j == 0:
                    strDescription = strDescription[1:]
                else:
                    strDescription = strDescription[:j] + ' ' + strDescription[j + 1:]

        strDescription = strDescription.strip('\n')

        return strDescription

    def cleanupColumnsDS(self, DataFrameIn, featureNames, label):

        featureNamesExt = copy.copy(list(featureNames))
        featureNamesExt.append(label)

        #         DataFrameOut=DataFrameIn.copy(deep=True)
        listFrameOut = []
        listIndexDescrCol = []
        for itemList in featureNamesExt:
            indexDescrCol = DataFrameIn.columns.get_loc(str(itemList))
            listIndexDescrCol.append(indexDescrCol)

        lenDataFrame = len(DataFrameIn)

        for i in range(lenDataFrame):
            mlsRecord = DataFrameIn.iloc[i, :].values
            boolOK = True
            for indexDescrCol in listIndexDescrCol:

                try:
                    objField = mlsRecord[int(indexDescrCol)]
                    if isinstance(objField, str):
                        strField = str(mlsRecord[int(indexDescrCol)]).upper().strip()
                    else:
                        strField = "_NA"
                    if strField == "":
                        strField = "_NA"

                except (RuntimeError, TypeError, NameError):
                    strField = "_NA"

                if strField != "_NA":
                    strField = self.cleanupString(strField)

                mlsRecord[int(indexDescrCol)] = strField

            listFrameOut.append(mlsRecord)

        DataFrameOut = pd.DataFrame(listFrameOut, columns=DataFrameIn.columns)

        return DataFrameOut

    def separateTrainAndTestRecordsDS(self, test_fraction):

        self.train_testData = self.data

        self.train_testData["test"] = self.train_testData.apply(lambda x: 1 if np.random.rand() < test_fraction else 0,
                                                                axis=1)
        self.trainingData = self.train_testData[self.train_testData["test"] == 0].copy(deep=True)
        self.testingData = self.train_testData[self.train_testData["test"] == 1].copy(deep=True)

    def setX_featuresDS(self, DataFrameIn, featureNames):

        DataFrameOut = DataFrameIn.copy(deep=True)
        DataFrameOut["X"] = ""

        listOfNewColumns = DataFrameOut.columns

        lenDataFrame = len(DataFrameOut)
        indexXCol = DataFrameOut.columns.get_loc("X")
        listIndexDescrCol = []
        for itemList in featureNames:
            indexDescrCol = int(DataFrameOut.columns.get_loc(str(itemList)))
            listIndexDescrCol.append(indexDescrCol)

        listIndexDescrCol.sort()
        lenListIndexDescrCol = len(listIndexDescrCol)
        listFrameOut = []
        for i in range(lenDataFrame):
            mlsRecord = DataFrameOut.iloc[i, :].values
            #             boolOK=True
            strSum = ""
            iNumOfNAs = 0
            for indexDescrCol in listIndexDescrCol:

                strField = str(mlsRecord[indexDescrCol])
                if strField == "_NA":
                    #                     boolOK=False
                    #                     break
                    iNumOfNAs += 1
                else:
                    strSum += strField + " "
            strSum = strSum.strip()
            #             if boolOK:
            if iNumOfNAs == lenListIndexDescrCol:
                continue
            else:
                mlsRecord[indexXCol] = strSum
                listFrameOut.append(mlsRecord)

        DataFrameOut = pd.DataFrame(listFrameOut, columns=listOfNewColumns)

        return DataFrameOut

    def setY(self, yFeature):
        self.y = yFeature


# ******************* NEW CLASS FOR ENGINEERING *****************************
class ItemClassifier:
    def __init__(self, strConfigFileName, boolRecreate):

        #    ************* Getting parameters **************************
        config = configparser.ConfigParser()
        config.read(strConfigFileName)

        #    ************* Paths *********************
        self.strDATASET_PATH = config.get('Paths', 'Training_Data_Path')
        self.strDATA_FOLDER = os.path.dirname(self.strDATASET_PATH)
        if self.strDATA_FOLDER != "":
            self.strDATA_FOLDER += '/'
        strBaseName = os.path.basename(self.strDATASET_PATH)
        listBaseName = list(strBaseName.split('.'))
        self.strFILE_NAME = str(listBaseName[0]).strip()
        self.strDATASET_PATH_VER = config.get('Paths', 'Predicted_Data_Path')
        self.strDATA_FOLDER_VER = os.path.dirname(self.strDATASET_PATH_VER)
        if self.strDATA_FOLDER_VER != "":
            self.strDATA_FOLDER_VER += '/'
        strBaseName = os.path.basename(self.strDATASET_PATH_VER)
        listBaseName = list(strBaseName.split('.'))
        self.strFILE_NAME_VER = str(listBaseName[0]).strip()
        self.strDATASET_PATH_VER_SAVE = config.get('Paths', 'Predicted_Data_Path_Saved')

        #    ********************* Titles *************************
        self.strALGORITHM_TITLE = config.get('Titles', 'Prediction_Algorithm_Title')

        #    **************** Fields ***********************
        strFeatures = config.get('Fields', 'Training_Column_Titles')
        listTemp = list(strFeatures.split(","))
        self.listFEATURE_COLUMNS = [x.strip() for x in listTemp]
        self.strColYSName = config.get('Fields', 'Predicted_Column_Title')

        #    ******************* Numbers ******************************
        self.strBATCH_SIZE = config.get('Numbers', 'Training_Batch_Length')
        self.strMAX_DOCUMENT_LENGTH = config.get('Numbers', 'Maximum_String_Length')
        self.strEMBEDDING_SIZE = config.get('Numbers', 'Word_Embedding_Size')
        self.strVALIDATION_PERCENTS = config.get('Numbers', 'Validation_Percents')
        self.strNUMBER_OF_STEPS = config.get('Numbers', 'Number_of_Steps')

        #    ****************** Booleans **************************
        self.boolUSE_BATCH_SIZE = config.getboolean('Booleans', 'Using_Batch_Sampling')

        for i in range(len(listClassificationAlgorithms)):
            if str(listClassificationAlgorithms[i]) == self.strALGORITHM_TITLE:
                break

        self.intALGORITHM_INDEX = i
        self.boolRecreate = boolRecreate

        self.mls = MLSituation()
        self.listOfDFColumns = []
        self.listOfDFColumnsVer = []

    #  ********** Load training data ****************
    def loadTrainingData(self):

        strErrorMessage = ""

        strDataPathInit = self.strDATASET_PATH
        if not os.path.exists(strDataPathInit):
            strErrorMessage = 'Error: File: ' + strDataPathInit + ' does not exist.'
            return strErrorMessage

        strDataPathPkl = self.strDATA_FOLDER + self.strFILE_NAME + '_DF.pkl'
        self.listOfDFColumns = []

        if (not os.path.exists(strDataPathPkl)) or self.boolRecreate:

            self.mls.data = pd.read_excel(strDataPathInit, encoding="ISO-8859-1", keep_default_na=False)

            listTemp = list(self.mls.data.columns)
            self.listOfDFColumns = [x.lower().strip().replace(" ", "_") for x in listTemp]

            #   ******* Pickle initial dataframe ***********
            self.mls.data.columns = self.listOfDFColumns
            self.mls.data.to_pickle(strDataPathPkl)

        else:

            self.mls.data = pd.read_pickle(strDataPathPkl)
            self.listOfDFColumns = copy.copy(list(self.mls.data.columns))

        return strErrorMessage

    def train(self):

        strErrorMessage = ""
        dictResults = {}

        #         if self.strALGORITHM_TITLE=='Bag-of-words' and self.boolUSE_BATCH_SIZE==True:
        #           strErrorMessage='Warning: Only Batch Size = Number of Rows is implemented now.'
        #           return strErrorMessage,dictResults

        if self.strALGORITHM_TITLE == 'Recurrent_NN':
            strErrorMessage = 'Warning: RNN classifier has not yet achieved acceptable accuracy. Please select another method.'
            return strErrorMessage, dictResults

        boolYIn = self.strColYSName in self.listOfDFColumns
        boolFound = True
        for itemList in self.listFEATURE_COLUMNS:
            itemListStripped = str(itemList).strip()
            if itemListStripped not in self.listOfDFColumns:
                boolFound = False
                break
        if not boolFound or not boolYIn:
            strErrorMessage = 'Error: Column titles selected for training are not in training file.'
            return strErrorMessage, dictResults

        if self.strALGORITHM_TITLE == 'Bag-of-words':

            strClassifierPathPkl = self.strDATA_FOLDER + self.strFILE_NAME + '_Clf_VPr_LCl.pkl'
            model_dir = self.strDATA_FOLDER + self.strFILE_NAME + '/' + 'ClfModel'
            if os.path.exists(model_dir) and (not self.boolRecreate):
                dictResults['classifier_pickle_path'] = strClassifierPathPkl
                dictResults['model_directory'] = model_dir
                return strErrorMessage, dictResults

            # ********** Delete earlier model folder **************
            if os.path.exists(model_dir):
                shutil.rmtree(model_dir, ignore_errors=True)

        elif self.strALGORITHM_TITLE == 'Multinomial_NB':
            strClassifierPathPkl = self.strDATA_FOLDER + self.strFILE_NAME + '_Tv_Mb_Mpr.pkl'
            model_dir = ""
            if os.path.exists(strClassifierPathPkl) and (not self.boolRecreate):
                dictResults['classifier_pickle_path'] = strClassifierPathPkl
                dictResults['model_directory'] = model_dir
                return strErrorMessage, dictResults

            # ******* Delete earlier file *****************
            if os.path.exists(strClassifierPathPkl):
                os.remove(strClassifierPathPkl)

                #  ****** Clean DataFrame *****************
        self.mls.data = self.mls.cleanupColumnsDS(self.mls.data, self.listFEATURE_COLUMNS, self.strColYSName)

        #  ********** Concatenate columns ************
        self.mls.data = self.mls.setX_featuresDS(self.mls.data, self.listFEATURE_COLUMNS)

        #   *********** Form training and validation dataframes ***********
        testShare = int(self.strVALIDATION_PERCENTS) / 100.
        self.mls.separateTrainAndTestRecordsDS(testShare)

        self.mls.setY(self.strColYSName)

        #   ************ Build dictionary and list of labels ****************
        dictClasslabels = {}
        listClasslabels = []
        iLabel = 0
        iNumberOfPassed = 0

        for strRow in self.mls.data[self.strColYSName]:
            if strRow != "None" and strRow != "" and strRow != "_NA":
                iNumberOfPassed = iNumberOfPassed + 1
                if strRow not in dictClasslabels:
                    dictClasslabels[strRow] = np.int32(iLabel)
                    listClasslabels.append(strRow)
                    iLabel = iLabel + 1

        lenDictClasslabels = len(listClasslabels)  # Number of classes

        if lenDictClasslabels == 0:
            strErrorMessage = 'Error: No meaningful class labels.'
            return strErrorMessage, dictResults

        if self.strALGORITHM_TITLE == 'Multinomial_NB':

            self.mls.setDictionary()
            self.mls.setMapper()

            # Just to confirm that things were created correctly
            self.mls.testingData["X"]

            # Most important step in the text analysis. This is to create the matrix with term frequency times inverse document
            # frequency
            # from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer
            #             cv = CountVectorizer(vocabulary=self.mls.dictionary)
            #             tt = TfidfTransformer()

            tv = TfidfVectorizer(vocabulary=self.mls.dictionary)
            xfeatures = tv.fit_transform(self.mls.trainingData["X"])
            xfeatures.shape

            # This is the actual training of the model. We use the Naive Bayes Multinomial algorithm.
            # from sklearn.naive_bayes import MultinomialNB
            mb = MultinomialNB()
            mb.fit(xfeatures, self.mls.mapper.transform(self.mls.trainingData[self.mls.y]))

            # And now we test the algorithm on the training data
            # import numpy as np
            predxfeatures = tv.fit_transform(self.mls.testingData["X"])
            self.mls.testingData["nbPredicted"] = self.mls.mapper.inverse_transform(mb.predict(predxfeatures))
            self.mls.testingData["nbScore"] = (self.mls.testingData["nbPredicted"] == self.mls.testingData[self.mls.y])

            lenTest = len(self.mls.testingData)
            num_of_trues = 0
            for i in range(lenTest):
                if self.mls.testingData.iloc[i]["nbScore"]:
                    num_of_trues += 1

            score = num_of_trues / lenTest;
            #             print('Accuracy: {0:f}'.format(score))

            # **********  Pickle classifier, vocab_processor and list of classes *************
            with open(strClassifierPathPkl, "wb") as fp:
                pickle.dump([tv, mb, self.mls.mapper], fp)

            # print('Finished Bayes classification')

            n_words = len(self.mls.dictionary)

        elif self.strALGORITHM_TITLE == 'Bag-of-words':

            #  ********** Form lists of Xs and Ys for training **********
            lenTrain = len(self.mls.trainingData)
            listXtrain = []
            listYTrain = []
            for i in range(lenTrain):
                lscItem = self.mls.trainingData.iloc[i][self.strColYSName]
                if lscItem in dictClasslabels:
                    listXtrain.append(self.mls.trainingData.iloc[i]['X'])
                    listYTrain.append(dictClasslabels[lscItem])

                    #  ********** Form list of Xs and Ys for verification/validation/testing *******
            lenTest = len(self.mls.testingData)
            listXtest = []
            listYtest = []
            for i in range(lenTest):
                lscItem = self.mls.testingData.iloc[i][self.strColYSName]
                if lscItem in dictClasslabels:
                    listXtest.append(self.mls.testingData.iloc[i]['X'])
                    listYtest.append(dictClasslabels[lscItem])

                    #   ********** Define input arrays for training/testing ***********
            x_train = pd.Series(listXtrain)
            y_train = pd.Series(listYTrain)
            x_test = pd.Series(listXtest)
            y_test = pd.Series(listYtest)

            #  ********* Define if batch size is different from raw data array length ********
            if self.boolUSE_BATCH_SIZE:
                iBatchSizeTemp = int(self.strBATCH_SIZE)
            else:
                iBatchSizeTemp = 0

                #   ******** Do training and testing/verification/validation and return classifier, vocab_processor and validation score *********
            classifier, vocab_processor, score, _ = tc.main(x_train, y_train, x_test, y_test, None, lenDictClasslabels,
                                                            self.intALGORITHM_INDEX,
                                                            None, None, model_dir, iBatchSizeTemp,
                                                            int(self.strMAX_DOCUMENT_LENGTH),
                                                            int(self.strEMBEDDING_SIZE), int(self.strNUMBER_OF_STEPS))

            # **********  Pickle classifier, vocab_processor and list of classes *************
            with open(strClassifierPathPkl, "wb") as fp:
                pickle.dump([classifier, vocab_processor, listClasslabels], fp)

            n_words = len(vocab_processor.vocabulary_)

            #  ********** Show validation accuracy ************
        errorRate = (1 - score) * 100
        strErrorRate = '{:{width}.{prec}f}'.format(errorRate, width=5, prec=2)
        dictResults['error_rate_percents'] = strErrorRate

        dictResults['vocabulary_size'] = str(n_words)
        dictResults['number_of_classes'] = str(lenDictClasslabels)
        dictResults['number_of_rows_passed'] = str(iNumberOfPassed)

        dictResults['classifier_pickle_path'] = strClassifierPathPkl
        dictResults['model_directory'] = model_dir

        dictResults['classification_algorithm'] = self.strALGORITHM_TITLE

        return strErrorMessage, dictResults

    def loadPredictionData(self):

        strErrorMessage = ""

        strDataPathInitVer = self.strDATASET_PATH_VER
        splitFILE_NAME_VER = self.strFILE_NAME_VER.split('.')
        strDataPathPklVer = self.strDATA_FOLDER_VER + splitFILE_NAME_VER[0] + '_DF_Ver.pkl'
        if (not os.path.exists(strDataPathPklVer)) or self.boolRecreate:

            #  *************** Read from Excel table to pandas DataFrame ****************
            self.mls.verifyData = pd.read_excel(strDataPathInitVer, encoding="ISO-8859-1", keep_default_na=False)

            listTemp = list(self.mls.verifyData.columns)
            self.listOfDFColumnsVer = [x.lower().replace(" ", "_") for x in listTemp]

            boolYIn = self.strColYSName in self.listOfDFColumnsVer
            boolFound = True
            for itemList in self.listFEATURE_COLUMNS:
                if str(itemList) not in self.listOfDFColumnsVer:
                    boolFound = False
                    break

            if not boolFound or not boolYIn:
                strErrorMessage = 'Error: Column titles selected for prediction are not in training file.'
                return strErrorMessage

            # *********** Save DataFrame read *****************
            self.mls.verifyData.columns = self.listOfDFColumnsVer
            self.mls.verifyData.to_pickle(strDataPathPklVer)
        else:
            self.mls.verifyData = pd.read_pickle(strDataPathPklVer)
            self.listOfDFColumnsVer = copy.copy(list(self.mls.verifyData.columns))

        return strErrorMessage

    def predict(self):

        strErrorMessage = ""
        dictResults = {}

        #  ****** Clean DataFrame *****************
        self.mls.verifyData = self.mls.cleanupColumnsDS(self.mls.verifyData, self.listFEATURE_COLUMNS,
                                                        self.strColYSName)

        #  ********* Concatenate columns *************
        self.mls.verifyData = self.mls.setX_featuresDS(self.mls.verifyData, self.listFEATURE_COLUMNS)

        #  ********* Form verification/validation X list
        lenVer = len(self.mls.verifyData)
        listXver = []
        for i in range(lenVer):
            listXver.append(self.mls.verifyData.iloc[i]['X'])
        x_ver = pd.Series(listXver)

        if self.strALGORITHM_TITLE == 'Multinomial_NB':

            #  *********** Unpickle trained parameter arrays ************
            strClassifierPathPkl = self.strDATA_FOLDER + self.strFILE_NAME + '_Tv_Mb_Mpr.pkl'
            if not os.path.exists(strClassifierPathPkl):
                strErrorMessage = 'Error: Training is not yet done.'
                return strErrorMessage, dictResults

            with open(strClassifierPathPkl, "rb") as fp:
                listOfObjects = pickle.load(fp)

            tv = listOfObjects[0]
            mb = listOfObjects[1]
            mapper = listOfObjects[2]

            predxfeatures = tv.fit_transform(listXver)

            #             mls.verifyData = mapper.inverse_transform(mb.predict(predxfeatures))
            listOfYs = mapper.inverse_transform(mb.predict(predxfeatures))

            #  ********** Fill in predicted labels/classes ***********
            indexLevSubCom = self.mls.verifyData.columns.get_loc(self.strColYSName)
            lenVerDataFrame = len(self.mls.verifyData)
            for i in range(lenVerDataFrame):
                self.mls.verifyData.iat[i, indexLevSubCom] = listOfYs[i]


        elif self.strALGORITHM_TITLE == 'Bag-of-words':

            #  *********** Unpickle trained parameter arrays ************
            strClassifierPathPkl = self.strDATA_FOLDER + self.strFILE_NAME + '_Clf_VPr_LCl.pkl'
            if not os.path.exists(strClassifierPathPkl):
                strErrorMessage = 'Error: Training is not yet done.'
                return strErrorMessage, dictResults

            with open(strClassifierPathPkl, "rb") as fp:
                listOfObjects = pickle.load(fp)

            classifier = listOfObjects[0]
            vocab_processor = listOfObjects[1]
            listClasslabels = listOfObjects[2]

            #  ********** Check model's availability ******
            model_dir = self.strDATA_FOLDER + self.strFILE_NAME + '/' + 'ClfModel'
            if not os.path.exists(model_dir):
                self.ui.textEditStatus.setText('Directory: ' + model_dir + ' is unavailable.')
                strErrorMessage = 'Error: Directory: ' + model_dir + ' is unavailable.'
                return strErrorMessage, dictResults

            iNumOfClasses = len(listClasslabels)

            if self.boolUSE_BATCH_SIZE:
                iBatchSizeTemp = int(self.strBATCH_SIZE)
            else:
                iBatchSizeTemp = 0

                #  ********** Do prediction *************
            _, _, _, y_predictedVer, probs = tc.main(None, None, None, None, x_ver, iNumOfClasses,
                                                     self.intALGORITHM_INDEX, classifier, vocab_processor,
                                                     model_dir, iBatchSizeTemp, int(self.strMAX_DOCUMENT_LENGTH),
                                                     int(self.strEMBEDDING_SIZE), int(self.strNUMBER_OF_STEPS))

            #  ********** Fill in predicted labels/classes ***********
            indexLevSubCom = self.mls.verifyData.columns.get_loc(self.strColYSName)
            lenVerDataFrame = len(self.mls.verifyData)

            for i in range(lenVerDataFrame):
                labelIndex = y_predictedVer[i]
                row_ind = self.mls.verifyData.index[i]
                # print('row ind ',row_ind, ' i ',i)
                self.mls.verifyData.iat[i, indexLevSubCom] = listClasslabels[labelIndex]
                # print('FOR ROW ', i, " TOP PRED ACTION ", listClasslabels[labelIndex])

                # get the next top 3
                top_prob_indices = probs[i].argsort()[-3:][::-1]  # get top 3 probs
                # print('FOR ROW ', i, " top_prob_indices ", top_prob_indices)
                ind = top_prob_indices[0]
                # print('TOP PROB ', probs[i, ind])
                self.mls.verifyData.loc[row_ind, 'First_Top_Probability'] = probs[i, ind]
                # print("self.mls.verifyData.loc[row_ind,'First_Top_Probability'] ", self.mls.verifyData.loc[row_ind,'First_Top_Probability'])


                try:
                    # get 2nd most common action
                    ind = top_prob_indices[1]
                    self.mls.verifyData.loc[row_ind, 'Second_Top_Probability'] = probs[i, ind]
                    self.mls.verifyData.loc[row_ind, 'Second_Top_Prediction'] = listClasslabels[ind]

                except Exception as e:
                    print("TOP 2ND ACTION ", e)
                    pass

                try:
                    # get 2nd most common action
                    ind = top_prob_indices[2]
                    self.mls.verifyData.loc[row_ind, 'Third_Top_Probability'] = probs[i, ind]
                    self.mls.verifyData.loc[row_ind, 'Third_Top_Prediction'] = listClasslabels[ind]

                except Exception as e:
                    print("TOP 3rd ACTION ", e)
                    pass



                    # *********** Save new DataFrame into Excel file ***********
        if self.strDATASET_PATH_VER_SAVE == "":
            self.splitFILE_NAME_VER = self.strFILE_NAME_VER.split('.')
            strDataPathToExcel = self.strDATA_FOLDER_VER + self.splitFILE_NAME_VER[0] + '_LDSC.xlsx'
            self.strDATASET_PATH_VER_SAVE = strDataPathToExcel

        self.mls.verifyData = self.mls.verifyData.drop('X', 1)

        #    ************** Save prediction results to Excel ****************
        self.mls.verifyData.to_excel(self.strDATASET_PATH_VER_SAVE, index=False)

        #    ************** Save prediction results to pickle ****************
        strBaseName = os.path.basename(self.strDATASET_PATH_VER_SAVE)
        listBaseName = list(strBaseName.split('.'))
        self.strFILE_NAME_VER_SAVE = str(listBaseName[0]).strip()

        strDataPathPklVerSave = os.path.dirname(
            self.strDATASET_PATH_VER_SAVE) + "/" + self.strFILE_NAME_VER_SAVE + '_DF.pkl'
        self.mls.verifyData.to_pickle(strDataPathPklVerSave)

        #  ************ Update GUI table ***********************
        iNumberOfRecords = len(self.mls.verifyData)

        dictResults['number_of_rows_passed'] = str(iNumberOfRecords)
        dictResults['predicted_file_path'] = self.strDATASET_PATH_VER_SAVE

        return strErrorMessage, dictResults








