# Copyright (c) 2016, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import


import os
import re

# import operator
# import shutil
# import subprocess
# import tempfile
# import sys
# import utils

import time
import numpy as np

from .train import TrainTask
from utils import subclass, override, constants

import tensorflow as tf

# NOTE: Increment this everytime the pickled object changes
PICKLE_VERSION = 1

# Constants
TENSORFLOW_MODEL_FILE = 'network.py'
TENSORFLOW_SNAPSHOT_PREFIX = 'snapshot'
TIMELINE_PREFIX = 'timeline'

CONFIG_FILE='config.ini'

from . import ItemClassifierDS  #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_array_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def subprocess_visible_devices(gpus):
    """
    Calculates CUDA_VISIBLE_DEVICES for a subprocess
    """
    if not isinstance(gpus, list):
        raise ValueError('gpus should be a list')
    gpus = [int(g) for g in gpus]

    old_cvd = os.environ.get('CUDA_VISIBLE_DEVICES', None)
    if old_cvd is None:
        real_gpus = gpus
    else:
        map_visible_to_real = {}
        for visible, real in enumerate(old_cvd.split(',')):
            map_visible_to_real[visible] = int(real)
        real_gpus = []
        for visible_gpu in gpus:
            real_gpus.append(map_visible_to_real[visible_gpu])
    return ','.join(str(g) for g in real_gpus)


@subclass
class TensorflowTrainTask(TrainTask):
    """
    Trains a tensorflow model
    """

    TENSORFLOW_LOG = 'tensorflow_output.log'

    def __init__(self, **kwargs):
        """
        Arguments:
        network -- a NetParameter defining the network
        """
        super(TensorflowTrainTask, self).__init__(**kwargs)

#         # save network description to file
#         with open(os.path.join(self.job_dir, TENSORFLOW_MODEL_FILE), "w") as outfile:    #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#             outfile.write(self.network)

        self.pickver_task_tensorflow_train = PICKLE_VERSION

        self.current_epoch = 0

        self.loaded_snapshot_file = None
        self.loaded_snapshot_epoch = None

        self.classifier = None
        self.solver = None

#         self.model_file = TENSORFLOW_MODEL_FILE
        self.model_file = CONFIG_FILE       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        self.train_file = constants.TRAIN_DB
#         self.val_file = constants.VAL_DB
        self.snapshot_prefix = TENSORFLOW_SNAPSHOT_PREFIX
#         self.log_file = self.TENSORFLOW_LOG
        
        self.entry_count = 0        #    !!!!!!!!!!!!!!!!!!!!!!
        
    def __getstate__(self):
        state = super(TensorflowTrainTask, self).__getstate__()

        # Don't pickle these things

        if 'classifier' in state:
            del state['classifier']
        if 'tensorflow_log' in state:
            del state['tensorflow_log']

        return state

    def __setstate__(self, state):
        super(TensorflowTrainTask, self).__setstate__(state)

        # Make changes to self
        self.loaded_snapshot_file = None
        self.loaded_snapshot_epoch = None

        # These things don't get pickled

        self.classifier = None

    # Task overrides
    @override
    def name(self):
        return 'Train Tensorflow Model'

    @override
    def before_run(self):
        super(TensorflowTrainTask, self).before_run()
#         self.tensorflow_log = open(self.path(self.TENSORFLOW_LOG), 'a')
        self.saving_snapshot = False
        self.receiving_train_output = False
        self.receiving_val_output = False
        self.last_train_update = None
        self.displaying_network = False
        self.temp_unrecognized_output = []
        return True

    @override
    def get_snapshot(self, epoch=-1, download=False, frozen_file=False):
        """
        return snapshot file for specified epoch
        """
        snapshot_pre = None

        if len(self.snapshots) == 0:
            return "no snapshots"

        if epoch == -1 or not epoch:
            epoch = self.snapshots[-1][1]
            snapshot_pre = self.snapshots[-1][0]
        else:
            for f, e in self.snapshots:
                if e == epoch:
                    snapshot_pre = f
                    break
        if not snapshot_pre:
            raise ValueError('Invalid epoch')
        if download:
            snapshot_file = snapshot_pre + ".data-00000-of-00001"
            meta_file = snapshot_pre + ".meta"
            index_file = snapshot_pre + ".index"
            snapshot_files = [snapshot_file, meta_file, index_file]
        elif frozen_file:
            snapshot_files = os.path.join(os.path.dirname(snapshot_pre), "frozen_model.pb")
        else:
            snapshot_files = snapshot_pre

        return snapshot_files



    @staticmethod
    def preprocess_output_tensorflow(line):
        """
        Takes line of output and parses it according to tensorflow's output format
        Returns (timestamp, level, message) or (None, None, None)
        """
        # NOTE: This must change when the logging format changes
        # LMMDD HH:MM:SS.MICROS pid file:lineno] message
        match = re.match(r'(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})\s\[(\w+)\s*]\s+(\S.*)$', line)
        if match:
            timestamp = time.mktime(time.strptime(match.group(1), '%Y-%m-%d %H:%M:%S'))
            level = match.group(2)
            message = match.group(3)
            if level == 'INFO':
                level = 'info'
            elif level == 'WARNING':
                level = 'warning'
            elif level == 'ERROR':
                level = 'error'
            elif level == 'FAIL':  # FAIL
                level = 'critical'
            return (timestamp, level, message)
        else:
            # self.logger.warning('Unrecognized task output "%s"' % line)
            return (None, None, None)

    def send_snapshot_update(self):
        """
        Sends socketio message about the snapshot list
        """
        # TODO: move to TrainTask
        from webapp import socketio

        socketio.emit('task update', {'task': self.html_id(),
                                      'update': 'snapshots',
                                      'data': self.snapshot_list()},
                      namespace='/jobs',
                      room=self.job_id)

    # TrainTask overrides
    @override
    def after_run(self):
        if self.temp_unrecognized_output:
            if self.traceback:
                self.traceback = self.traceback + ('\n'.join(self.temp_unrecognized_output))
            else:
                self.traceback = '\n'.join(self.temp_unrecognized_output)
                self.temp_unrecognized_output = []
#         self.tensorflow_log.close()

    @override
    def detect_timeline_traces(self):
        timeline_traces = []
        for filename in os.listdir(self.job_dir):
            # find timeline jsons
            match = re.match(r'%s_(.*)\.json$' % TIMELINE_PREFIX, filename)
            if match:
                step = int(match.group(1))
                timeline_traces.append((os.path.join(self.job_dir, filename), step))
        self.timeline_traces = sorted(timeline_traces, key=lambda tup: tup[1])
        return len(self.timeline_traces) > 0

#     @override
#     def detect_snapshots(self):
#         self.snapshots = []
#         snapshots = []
#         for filename in os.listdir(self.job_dir):
#             # find models
#             match = re.match(r'%s_(\d+)\.?(\d*)\.ckpt\.index$' % self.snapshot_prefix, filename)
#             if match:
#                 epoch = 0
#                 # remove '.index' suffix from filename
#                 filename = filename[:-6]
#                 if match.group(2) == '':
#                     epoch = int(match.group(1))
#                 else:
#                     epoch = float(match.group(1) + '.' + match.group(2))
#                 snapshots.append((os.path.join(self.job_dir, filename), epoch))
#         self.snapshots = sorted(snapshots, key=lambda tup: tup[1])
#         return len(self.snapshots) > 0

    @override
    def est_next_snapshot(self):
        # TODO: Currently this function is not in use. Probably in future we may have to implement this
        return None

    
    def get_layer_statistics(self, data):
        """
        Returns statistics for the given layer data:
            (mean, standard deviation, histogram)
                histogram -- [y, x, ticks]

        Arguments:
        data -- a np.ndarray
        """
        # These calculations can be super slow
        mean = np.mean(data)
        std = np.std(data)
        y, x = np.histogram(data, bins=20)
        y = list(y)
        ticks = x[[0, len(x)/2, -1]]
        x = [(x[i]+x[i+1])/2.0 for i in range(len(x)-1)]
        ticks = list(ticks)
        return (mean, std, [y, x, ticks])


    def has_model(self):
        """
        Returns True if there is a model that can be used
        """
        return len(self.snapshots) != 0

    @override
    def get_model_files(self):
        """
        return paths to model files
        """
#         return {"Network": self.model_file}
        return {"Config_File": self.model_file}     #    !!!!!!!!!!!!!!!!!!!!!!!!!!

    @override
    def get_task_stats(self, epoch=-1):
        """
        return a dictionary of task statistics
        """

        stats = {

            "model file": self.model_file,
            "framework": "tensorflow",

        }

        return stats
    
    #    ????????????????????????????????????????????????
    def runItemizer(self,strConfigFileName,stage):

        boolRecreate=False
        dictResults={}

#  *********** Instantiate class with config file filled out ****************
        myItemClassifier=ItemClassifierDS.ItemClassifier(strConfigFileName,boolRecreate)

        if stage=="training":
#    **************** Load training data (Excel file currently) **********************
            strErrorMessage=myItemClassifier.loadTrainingData()
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                return strErrorMessage,dictResults
 
#    ************* Train classifier *************************
            strErrorMessage,dictResults=myItemClassifier.train()
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                return strErrorMessage,dictResults
 
#             print(str(dictResults))
            
            return strErrorMessage,dictResults
        
        else:
            
#    ************* Load data to be predicted (Excel file currently) *************************
            strErrorMessage=myItemClassifier.loadPredictionData()
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                return strErrorMessage,dictResults

#    ************* Predict classes (based on trained classifier) *************************
            strErrorMessage,dictResults=myItemClassifier.predict()
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                return strErrorMessage,dictResults                
                
#             print(str(dictResults))

            return strErrorMessage,dictResults            
               
    #    ????????????????????????????????????????????????
