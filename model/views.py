# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import io
import json
import math
import os
import tarfile
import zipfile

import flask
from flask import flash
import requests
import werkzeug.exceptions

from . import texts as model_texts
from . import ModelJob

from dataset import generic      #    !!!!!!!!!!!!!!!!!!!!!!!!!

import frameworks, extensions
from utils import auth
from utils.routing import request_wants_json, job_from_request, get_request_arg
from webapp import scheduler

from .texts import classification     #    !!!!!!!!!!!!!!!!
from .texts import generic as genTexts     #    !!!!!!!!!!!!!!!!!!!!
from extensions import data      #    !!!!!!!!!!!!!!!!!!!!!!!

blueprint = flask.Blueprint(__name__, __name__)

@blueprint.route('/<job_id>.json', methods=['GET'])
@blueprint.route('/<job_id>', methods=['GET'])
def show(job_id):
    """
    Show a ModelJob

    Returns JSON when requested:
        {id, name, directory, status, snapshots: [epoch,epoch,...]}
    """
    job = scheduler.get_job(job_id)
    if job is None:
        raise werkzeug.exceptions.NotFound('Job not found')

    related_jobs = scheduler.get_related_jobs(job)

    if request_wants_json():
        return flask.jsonify(job.json_dict(True))
    else:
        if isinstance(job, model_texts.TextClassificationModelJob):
            return classification.views.show(job, related_jobs=related_jobs)
        elif isinstance(job, model_texts.GenericTextModelJob):
            return genTexts.views.show(job, related_jobs=related_jobs)
        else:
            raise werkzeug.exceptions.BadRequest(
                'Invalid job type')

@blueprint.route('/summary', methods=['GET'])
def summary():
    """
    Return a short HTML summary of a DatasetJob
    """
    job = job_from_request()
    if isinstance(job, model_texts.TextClassificationModelJob):
        return classification.views.summary(job)        #    !!!!!!!!!!!!!!!!!!!!!!!!
    
    elif isinstance(job, model_texts.GenericTextModelJob):
        return classification.views.summary_model(job)         #    !!!!!!!!!!!!!!!!!!!!!
    elif isinstance(job, generic.GenericDatasetJob):
        return classification.views.summary(job)
    else:
        raise werkzeug.exceptions.BadRequest('Invalid job type')

@blueprint.route('/timeline_trace_data', methods=['POST'])
def timeline_trace_data():
    """
    Shows timeline trace of a model
    """
    job = job_from_request()
    step = get_request_arg('step')
    if step is None:
        raise werkzeug.exceptions.BadRequest('step is a required field')
    return job.train_task().timeline_trace(int(step))


@blueprint.route('/view-config/<extension_id>', methods=['GET'])
def view_config(extension_id):
    """
    Returns a rendering of a view extension configuration template
    """
    extension = extensions.view.get_extension(extension_id)
    if extension is None:
        raise ValueError("Unknown extension '%s'" % extension_id)
    config_form = extension.get_config_form()
    template, context = extension.get_config_template(config_form)
    return flask.render_template_string(template, **context)


class JobBasicInfo(object):

    def __init__(self, name, ID, status, time, framework_id):
        self.name = name
        self.id = ID
        self.status = status
        self.time = time
        self.framework_id = framework_id


class ColumnType(object):

    def __init__(self, name, has_suffix, find_fn):
        self.name = name
        self.has_suffix = has_suffix
        self.find_from_list = find_fn

    def label(self, attr):
        if self.has_suffix:
            return '{} {}'.format(attr, self.name)
        else:
            return attr


# def get_column_attrs():
#     job_outs = [set(j.train_task().train_outputs.keys() + j.train_task().val_outputs.keys())
#                 for j in scheduler.jobs.values() if isinstance(j, ModelJob)]
# 
#     return reduce(lambda acc, j: acc.union(j), job_outs, set())
