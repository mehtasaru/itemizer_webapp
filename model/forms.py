# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

import os

import flask
# from flask.ext.wtf import Form
from flask_wtf import Form      #    !!!!!!!!!!!!!!!!!!!!!!!!

import wtforms
from wtforms import validators

from config import config_value
from device_query import get_device, get_nvml_info
import utils
from utils import sizeof_fmt
from utils.forms import validate_required_iff
import frameworks


class ModelForm(Form):

    # Methods

#     def selection_exists_in_choices(form, field):
    def selection_exists_in_choices(self,form, field):        
        
        found = False
        for choice in field.choices:
            if choice[0] == field.data:
                found = True
        if not found:
            raise validators.ValidationError("Selected job doesn't exist. Maybe it was deleted by another user.")

#     def validate_file_exists(form, field):
    def validate_file_exists(self,form, field):     #    !!!!!!!!!!!!!!!!!!!!
        
        from_client = bool(form.python_layer_from_client.data)

        filename = ''
        if not from_client and field.type == 'StringField':
            filename = field.data

        if filename == '':
            return

        if not os.path.isfile(filename):
            raise validators.ValidationError('Server side file, %s, does not exist.' % filename)

    # Fields

    # The options for this get set in the view (since they are dynamic)
    dataset = utils.forms.SelectField(
        'Select Dataset',
        choices=[],
        tooltip="Choose the dataset to use for this model."
    )

    train_epochs = utils.forms.IntegerField(
        'Training epochs',
        validators=[
            validators.NumberRange(min=1)
        ],
        default=30,
        tooltip="How many passes through the training data?"
    )

    snapshot_interval = utils.forms.FloatField(
        'Snapshot interval (in epochs)',
        default=1,
        validators=[
            validators.NumberRange(min=0),
        ],
        tooltip="How many epochs of training between taking a snapshot?"
    )

    val_interval = utils.forms.FloatField(
        'Validation interval (in epochs)',
        default=1,
        validators=[
            validators.NumberRange(min=0)
        ],
        tooltip="How many epochs of training between running through one pass of the validation data?"
    )

    traces_interval = utils.forms.IntegerField(
        'Tracing Interval (in steps)',
        validators=[
            validators.NumberRange(min=0)
        ],
        default=0,
        tooltip="Generation of a timeline trace every few steps"
    )

    random_seed = utils.forms.IntegerField(
        'Random seed',
        validators=[
            validators.NumberRange(min=0),
            validators.Optional(),
        ],
        tooltip=('If you provide a random seed, then back-to-back runs with '
                 'the same model and dataset should give identical results.')
    )

    batch_accumulation = utils.forms.IntegerField(
        'Batch Accumulation',
        validators=[
            validators.NumberRange(min=1),
            validators.Optional(),
        ],
        tooltip=("Accumulate gradients over multiple batches (useful when you "
                 "need a bigger batch size for training but it doesn't fit in memory).")
    )

#   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    solver_type = utils.forms.SelectField(
        'Solver type',
        id='solver_type',
        choices=[
            ('Bag-of-words', 'Multilayer Perceptron (Bag-of-Words)'),
            ('Recurrent_NN', 'Recurrent Neural Network'),
            ('Multinomial_NB', 'Multinomial Naive Bayes Classifier'),            
        ],
        default='Bag-of-words',
        tooltip="What type of solver will be used?",
    ) 
    
    max_doc_length = utils.forms.IntegerField(
        'Maximum length (in tokens)',
        validators=[
            validators.NumberRange(min=1)
        ],
        default=50,
        tooltip="How many tokens are used in a concatenated token string?"
    )    
    
    word_embed_size = utils.forms.IntegerField(
        'Vector components per word/term',
        validators=[
            validators.NumberRange(min=1)
        ],
        default=200,
        tooltip="What is dimension of a vector presenting a word?"
    )     
    
    validation_percents = utils.forms.IntegerField(
        'Percentage of records for validation',
        validators=[
            validators.NumberRange(min=1)
        ],
        default=10,
        tooltip="How many records should be set aside when training?"
    ) 
    
    batch_size = utils.forms.MultiIntegerField(
        'Batch size',
        validators=[
            utils.forms.MultiNumberRange(min=1),
            utils.forms.MultiOptional(),
        ],
        default=0,        
        tooltip="How many records to process simultaneously? IF ZERO, THE WHOLE DATASET IS USED."
    )
    
    number_of_steps = utils.forms.IntegerField(
        'Number of steps to run',
        validators=[
            validators.NumberRange(min=1)
        ],
        default=100,
        tooltip="How many steps should calculations be run?"
    )     
#    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#     def validate_solver_type(form, field):
    def validate_solver_type(self,form, field):         #    !!!!!!!!!!!!!!!!!!!!!!
        
        fw = frameworks.get_framework_by_id(form.framework)
        if fw is not None:
            if not fw.supports_solver_type(field.data):
                raise validators.ValidationError(
                    'Solver type not supported by this framework')

    # Additional settings specific to selected solver

    # framework - hidden field, set by Javascript to the selected framework ID
    framework = wtforms.HiddenField(
        'framework',
        validators=[
            validators.AnyOf(
                [fw.get_id() for fw in frameworks.get_frameworks()],
                message='The framework you choose is not currently supported.'
            )
        ],
        default=frameworks.get_frameworks()[0].get_id()
    )

    pretrained_networks = utils.forms.SelectField(
        'Select Model',
        choices=[],
        tooltip="Choose the network to use for prediction."
    )  

    # Select one of several GPUs
    select_gpu = wtforms.RadioField(
        'Select which GPU you would like to use',
        choices=[('next', 'Next available')] + [(
            index,
            '#%s - %s (%s memory)' % (
                index,
                get_device(index).name,
                sizeof_fmt(
                    get_nvml_info(index)['memory']['total']
                    if get_nvml_info(index) and 'memory' in get_nvml_info(index)
                    else get_device(index).totalGlobalMem)
            ),
        ) for index in config_value('gpu_list').split(',') if index],
        default='next',
    )

    # Select N of several GPUs
    select_gpus = utils.forms.SelectMultipleField(
        'Select which GPU[s] you would like to use',
        choices=[(
            index,
            '#%s - %s (%s memory)' % (
                index,
                get_device(index).name,
                sizeof_fmt(
                    get_nvml_info(index)['memory']['total']
                    if get_nvml_info(index) and 'memory' in get_nvml_info(index)
                    else get_device(index).totalGlobalMem)
            ),
        ) for index in config_value('gpu_list').split(',') if index],
        tooltip="The job won't start until all of the chosen GPUs are available."
    )

    # XXX For testing
    # The Flask test framework can't handle SelectMultipleFields correctly
    select_gpus_list = wtforms.StringField('Select which GPU[s] you would like to use (comma separated)')

#     def validate_select_gpus(form, field):
    def validate_select_gpus(self,form, field):     #    !!!!!!!!!!!!!!!!!!!!!
        
        if form.select_gpus_list.data:
            field.data = form.select_gpus_list.data.split(',')

    # Use next available N GPUs
    select_gpu_count = wtforms.IntegerField('Use this many GPUs (next available)',
                                            validators=[
                                                validators.NumberRange(min=1, max=len(
                                                    config_value('gpu_list').split(',')))
                                            ],
                                            default=1,
                                            )

#     def validate_select_gpu_count(form, field):
    def validate_select_gpu_count(self,form, field):        #    !!!!!!!!!!!!!!!!!!!
        
        if field.data is None:
            if form.select_gpus.data:
                # Make this field optional
                field.errors[:] = []
                raise validators.StopValidation()

    model_name = utils.forms.StringField('Model Name',
                                         validators=[
                                             validators.DataRequired()
                                         ],
                                         tooltip="An identifier, later used to refer to this model in the Application."
                                         )

    group_name = utils.forms.StringField('Group Name',
                                         tooltip="An optional group name for organization on the main page."
                                         )

