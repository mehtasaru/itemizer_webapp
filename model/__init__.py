# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from .texts import (
    TextClassificationModelJob,
    GenericTextModelJob,
    TextModelJob,
)
from .job import ModelJob

__all__ = [
    'TextClassificationModelJob',
    'GenericTextModelJob',
    'TextModelJob',
    'ModelJob',
]
