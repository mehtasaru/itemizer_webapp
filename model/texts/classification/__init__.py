# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from .job import TextClassificationModelJob

__all__ = ['TextClassificationModelJob']
