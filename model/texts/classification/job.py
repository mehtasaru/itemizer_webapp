# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os.path

from ..job import TextModelJob
from utils import subclass, override

# NOTE: Increment this every time the pickled object changes
PICKLE_VERSION = 1


@subclass
class TextClassificationModelJob(TextModelJob):
    """
    A Job that creates a text model for a classification network
    """

    def __init__(self, **kwargs):
        
        self.dict_pretrained=kwargs['dict_pretrained']      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        del kwargs['dict_pretrained']           #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        self.model_id=self.dict_pretrained['job_id']
        super(TextClassificationModelJob, self).__init__(**kwargs)
#         self.pickver_job_model_text_classification = PICKLE_VERSION

    @override
    def job_type(self):
        return 'Text_Classification_Model'

