# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os
import re
import tempfile

import flask
import numpy as np
import werkzeug.exceptions

from .forms import TextClassificationModelForm
from .job import TextClassificationModelJob
import extensions, frameworks, utils        #    !!!!!!!!!!!!!!!!!!!!!!!!!!
from config import config_value
from status import Status
from utils import filesystem as fs
from utils.forms import fill_form_if_cloned, save_form_to_job
from utils.routing import request_wants_json, job_from_request
from webapp import scheduler

from dataset import GenericDatasetJob        #    !!!!!!!!!!!!!!!!!!!!!!!!
from model.texts.generic.job import GenericTextModelJob

from utils.routing import request_wants_json, job_from_request,get_request_arg       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!

from flask import request       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
from flask import send_file     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# import configparser     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!

blueprint = flask.Blueprint(__name__, __name__)

@blueprint.route('/new', methods=['GET'])
# @utils.auth.requires_login        #    !!!!!!!!!!!!!!!!
def new():
    
    global strPredictDatasetPath,strPredictModelDir,strPredictModelPicklePath
    
    strPredictDatasetPath=""
    strPredictModelDir=""
    strPredictModelPicklePath=""
    strJobDir=""
    strJobID=""
    
    """
    Return a form for a new TextClassificationModelJob
    """
    form = TextClassificationModelForm()
    form.dataset.choices = get_datasets()
    
    form.pretrained_networks.choices = get_pretrained_networks()

    # Is there a request to clone a job with ?clone=<job_id>
    fill_form_if_cloned(form)

    return flask.render_template('models/texts/classification/new.html',
                                 form=form,
                                 frameworks=frameworks.get_frameworks(),

                                 )


@blueprint.route('.json', methods=['POST'])
@blueprint.route('', methods=['POST'], strict_slashes=False)
# @utils.auth.requires_login(redirect=False)        #    !!!!!!!!!!!!!!!!!!!!!
def create():
    
    global strJobDir,strPredictModelDir,strPredictModelPicklePath,strJobID
    
    """
    Create a new TextClassificationModelJob

    Returns JSON when requested: {job_id,name,status} or {errors:[]}
    """
    
    form=TextClassificationModelForm(request.form)
    
#     form = TextClassificationModelForm()
#     form.dataset.choices = get_datasets()
    
#     prev_network_snapshots = get_previous_network_snapshots()

    # Is there a request to clone a job with ?clone=<job_id>
    fill_form_if_cloned(form)

#     if not form.validate_on_submit():
    if not form:        #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if request_wants_json():
            return flask.jsonify({'errors': form.errors}), 400
        else:
            return flask.render_template('models/texts/classification/new.html',
                                         form=form,
                                         frameworks=frameworks.get_frameworks(),
#                                          previous_network_snapshots=prev_network_snapshots,
                                         ), 400

    datasetJob = scheduler.get_job(form.dataset.data)
    if not datasetJob:
        raise werkzeug.exceptions.BadRequest(
            'Unknown dataset job_id "%s"' % form.dataset.data)
        
#     config = configparser.ConfigParser()        
#     
#     strConfigPath=strJobDir+"/"+"config.ini"
#           
# #     with open(strConfigPath, 'r') as configfile:
# #     config.read(configfile)
#     
#     config.read(strConfigPath)
#         
#     config['Paths']['Predicted_Data_Path'] = datasetJob.extension_userdata['train_data_file_data']
    
    # sweeps will be a list of the the permutations of swept fields
    # Get swept learning_rate
#     sweeps = [{'learning_rate': v} for v in form.learning_rate.data]
#     add_learning_rate = len(form.learning_rate.data) > 1
#     
#     form.standard_networks.data=form.standard_networks.default      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
#     # Add swept batch_size
#     sweeps = [dict(s.items() + [('batch_size', bs)]) for bs in form.batch_size.data for s in sweeps[:]]
#     add_batch_size = len(form.batch_size.data) > 1

    dict_pretrained={}
    dict_pretrained['job_id']=strJobID    
    dict_pretrained['job_dir']=strJobDir
    dict_pretrained['model_coefs_dir']=strPredictModelDir 
    dict_pretrained['model_pickle_path']=strPredictModelPicklePath            
    
#     n_jobs = len(sweeps)
    n_jobs=1
    gpu_count = 1
                
    jobs = []
    
#     for sweep in sweeps:
#         # Populate the form with swept data to be used in saving and
#         # launching jobs.
#         form.learning_rate.data = sweep['learning_rate']
#         
# #         form.batch_size.data = sweep['batch_size']
# 
#         # Augment Job Name
    extra = ''
#         if add_learning_rate:
#             extra += ' learning_rate:%s' % str(form.learning_rate.data[0])
            
#         if add_batch_size:
#             extra += ' batch_size:%d' % form.batch_size.data[0]

    job = None
    try:
        job = TextClassificationModelJob(
            username=utils.auth.get_username(),
            name=form.model_name.data + extra,
            group=form.group_name.data,
            dataset_id=datasetJob.id(),
            dict_pretrained=dict_pretrained,
        )
        # get handle to framework object
        fw = frameworks.get_framework_by_id(form.framework.data)

        job.tasks.append(fw.create_train_task(
            job=job,
            dataset=datasetJob,

            gpu_count=gpu_count,

            batch_size=form.batch_size.data[0],

            solver_type=form.solver_type.data,

        )
        )

        # Save form data with the job so we can easily clone it later.
        save_form_to_job(job, form)

        jobs.append(job)
        
        scheduler.running = True    #    !!!!!!!!!!!!!!!!!!!!!!!!
        scheduler.add_job(job)
        
        strErrorMessage=scheduler.main_thread()          #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if strErrorMessage!="":
            scheduler.delete_job(job)
            
            error_type = "Input data error."
            trace = None
            description = None
            status_code = 500
            return flask.render_template('error.html',
                                         title=error_type,
                                         message=strErrorMessage,
                                         description=description,
                                         trace=trace,
                                         ), status_code
        
        if n_jobs == 1:
            if request_wants_json():
                return flask.jsonify(job.json_dict())
            else:
                return flask.redirect(flask.url_for('model.views.show', job_id=job.id()))

    except Exception as e:
        if job:
            scheduler.delete_job(job)
        raise

    if request_wants_json():
        return flask.jsonify(jobs=[j.json_dict() for j in jobs])

    # If there are multiple jobs launched, go to the home page.
    return flask.redirect('/')

@blueprint.route('/new/dataset_table_line', methods=['GET','POST'])
def dataset_table_line(job_id=None):
    
    strTable =get_request_arg('job_id')
    
#     choices = get_datasets(extension_id)
    
    return  strTable

# def show(job, related_jobs=None):
#     """
#     Called from model.views.models_show()
#     """
#     return flask.render_template(
#         'models/texts/classification/show.html',
#         job=job,
#         framework_ids=[
#             fw.get_id()
#             for fw in frameworks.get_frameworks()
#         ],
#         related_jobs=related_jobs
#     )

def get_data_extensions():
    """
    return all enabled data extensions
    """
    data_extensions = {"all-default": "Default"}
    all_extensions = extensions.data.get_extensions()
    for extension in all_extensions:
        data_extensions[extension.get_id()] = extension.get_title()
    return data_extensions


def get_view_extensions():
    """
    return all enabled view extensions
    """
    view_extensions = {}
    all_extensions = extensions.view.get_extensions()
    for extension in all_extensions:
        view_extensions[extension.get_id()] = extension.get_title()
    return view_extensions

    
def show(job, related_jobs=None):
    """
    Called from model.views.models_show()
    """
    data_extensions = get_data_extensions()
    view_extensions = get_view_extensions()

    return flask.render_template(
        'models/texts/classification/show.html',
        job=job,
        data_extensions=data_extensions,
        view_extensions=view_extensions,
        related_jobs=related_jobs,
    )

@blueprint.route('/timeline_tracing', methods=['GET'])
def timeline_tracing():
    """
    Shows timeline trace of a model
    """
    job = job_from_request()

    return flask.render_template('models/timeline_tracing.html', job=job)

# def get_datasets():
#     return [(j.id(), j.name()) for j in sorted(
#         [j for j in scheduler.jobs.values() if isinstance(j, TextClassificationDatasetJob) and
#          (j.status.is_running() or j.status == Status.DONE)],
#         cmp=lambda x, y: cmp(y.id(), x.id())
#     )
#     ]
    
def get_datasets():
    jobs = [j for j in scheduler.jobs.values()
            if (isinstance(j, GenericDatasetJob)
            and (j.status.is_running() or j.status == Status.DONE))]

    return [(j.id(), j.name()) for j in sorted(jobs,key=lambda x: x.id())]      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!


def get_pretrained_networks():
    return [(j.id(), j.name()) for j in scheduler.jobs.values() if isinstance(j, GenericTextModelJob)]

def summary(job):
    
    global strPredictDatasetPath
    
    strPredictDatasetPath=job.extension_userdata['train_data_file_data' ]
    
    """
    Return a short HTML summary of a GenericDatasetJob
    """
    return flask.render_template('datasets/generic/summary.html', dataset=job)

def summary_model(job):
    
    global strPredictModelDir,strPredictModelPicklePath,strJobDir,strJobID
    
    strPredictModelDir=job.dict_train_results['model_directory']
    strPredictModelPicklePath=job.dict_train_results['classifier_pickle_path' ]
    strJobDir=job._dir
    strJobID=job._id
    
    """
    Return a short HTML summary of a GenericDatasetJob
    """
    return flask.render_template('datasets/generic/summary.html', dataset=job)

@blueprint.route('/download', methods=['GET','POST'])
def download():
    """
    Returns a gallery consisting of the texts of one of the dbs
    """
    job = job_from_request()
    
    strFilePath=job.dict_predict_results['predicted_file_path']
    strBaseName=os.path.basename(strFilePath)
 
    return send_file(strFilePath,attachment_filename=strBaseName)
