# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os.path

from ..job import TextModelJob
from utils import subclass, override

# NOTE: Increment this every time the pickled object changes
PICKLE_VERSION = 1


@subclass
class GenericTextModelJob(TextModelJob):
    """
    A Job that creates a text model for a generic network
    """

    def __init__(self, **kwargs):
        super(GenericTextModelJob, self).__init__(**kwargs)
        self.pickver_job_model_text_generic = PICKLE_VERSION

    @override
    def job_type(self):
        return 'Generic_Text_Model'

   
    def get_file_name(self):
        """
        Return file name
        """
        return self.dataset.extension_userdata['train_data_file.data']
    
    def get_lines_length(self):
        
        return str(len(self.dataset.extension_userdata['train_data_list']))    
    
    def create_db_tasks(self):
        return self.tasks
