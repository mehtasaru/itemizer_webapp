# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from ..forms import TextModelForm


class GenericTextModelForm(TextModelForm):
    """
    Defines the form used to create a new GenericTextModelJob
    """
    pass
