# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os
import re
import tempfile

import time

import flask
import werkzeug.exceptions

from .forms import GenericTextModelForm
from .job import GenericTextModelJob
import extensions, frameworks, utils
from config import config_value
from dataset import GenericDatasetJob    #    !!!!!!!!!!!!!!!!!!!!!!
from status import Status
from utils import filesystem as fs
from utils import constants
from utils.forms import fill_form_if_cloned, save_form_to_job
from utils.routing import request_wants_json, job_from_request,get_request_arg       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
from webapp import scheduler

import configparser     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
import copy

from flask import request       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# from spacy.lang.nb.lemmatizer import _verbs_wordforms


blueprint = flask.Blueprint(__name__, __name__)

cur_job_id=""

# @blueprint.route('/summary', methods=['GET','POST'])
# def summary(job):
#     """
#     Return a short HTML summary of an GenericTextModelJob
#     """
#     return flask.render_template('models/texts/generic/summary.html',dataset=job)

@blueprint.route('/summary/<extension_id>', methods=['GET'])
def summary(extension_id=None):
    
    global cur_job_id
    job = job_from_request()
    cur_job_id=job.id()
    
    return flask.render_template('models/texts/generic/summary.html',dataset=job)

@blueprint.route('/select_x/<extension_id>', methods=['GET'])
def select_x(extension_id=None):
    """
    Return a short HTML summary of a DatasetJob
    """
    
    global cur_job_id
    
    job = job_from_request()
    
    cur_job_id=job.id()
    
    if isinstance(job, GenericDatasetJob):
#         form=job.form_data['form']
        form=GenericTextModelForm(request.form)        
        
        
#         form,job=change_form_with_train_params(form,job)        #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
#         
#         # Is there a request to clone a job with ?clone=<job_id>
#         fill_form_if_cloned(form)
#         job.form_data['form']=form      #    !!!!!!!!!!!!!!!!!!!!

        listOfColumns=list(job.extension_userdata['train_columns_list'])
        listOfColumns=listOfColumns[1:]
        
    #    *********** Fill out list of X_column choices *************
        form.select_x_columns.choices=[]
        listOfXTuples=[]
        for itemList in listOfColumns:
            myTuple=(str(itemList),str(itemList))
            form.select_x_columns.choices.append(myTuple)       #    Fill choices !!!!!!!!!!!!!
            myTuple2=(str(itemList),"False")
            listOfXTuples.append(myTuple2)
            


        form.select_x_columns.data=listOfXTuples[0][0]      #    Initial column
        form.select_true_or_false.data=listOfXTuples[0][1]  #    Initial column's contents
        form.contents_x_selected.data=listOfXTuples[0][1]  #    Initial column's contents


        form.select_true_or_false.data= "False"   #    Initial column's contents
        form.contents_x_selected.data= "No features added yet. Select from Dropdown"   #    Initial column's contents


        fill_form_if_cloned(form)
    
        job.form_data['form']=form      #    !!!!!!!!!!!!!!!!!!!!
        
        job.save()      #    !!!!!!!!!!!!!!!!!

        return flask.render_template('models/texts/generic/select_x.html',dataset=job,form=form)
    
#         return flask.render_template(
#             'models/texts/generic/new.html',
#             extension_id=extension_id,
#             extension_title=extensions.data.get_extension(extension_id).get_title() if extension_id else None,
#             form=form,
#             frameworks=frameworks.get_frameworks(),
#     #         previous_network_snapshots=prev_network_snapshots,
#     
#             job=job         #    !!!!!!!!!!!!!!!!!!!!!!!!!
#         )
    
    
    else:
        raise werkzeug.exceptions.BadRequest('Invalid job type')

@blueprint.route('/new', methods=['GET'])       #    !!!!!!!!!!!!!!!!!!!!!!!!
@blueprint.route('/new/<extension_id>', methods=['GET'])
# @utils.auth.requires_login        #    !!!!!!!!!!!!!!!!!!!!
def new(extension_id=None):
    """
    Return a form for a new GenericTextModelJob
    """
    
    global cur_job_id

    time.sleep(1.05)
 
    form = GenericTextModelForm()
    form.dataset.choices = get_datasets(extension_id)
    print('form.dataset.choices  \n',form.dataset.choices)

    #if cur_job_id=="":
    print('BEFORE CURRENT JOB ID is ', cur_job_id)
    cur_job_id=form.dataset.choices[-1][0]
    print("AFTER cur_job_id ", cur_job_id)
#     job_id=form.dataset.choices[0][0]       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#     cur_job_id = copy.copy(job_id)
#     job = scheduler.get_job(job_id)


    print('NEW VIEWS.PY TRYING TO FIND ', cur_job_id)
    job = scheduler.get_job(cur_job_id)
    print('ABLE TO FIND JOB ', job.id())
    
    form,job=change_form_with_train_params(form,job)        #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
#     form.standard_networks.choices = []
#     prev_network_snapshots = get_previous_network_snapshots()

    # Is there a request to clone a job with ?clone=<job_id>
    fill_form_if_cloned(form)
    
    job.form_data['form']=form      #    !!!!!!!!!!!!!!!!!!!!
    
#     for i in range(len(form.dataset.choices )):
#         j_id=form.dataset.choices[i][0]
#         j_obj = scheduler.get_job(j_id)
#         j_obj.form_data['form']=form      #    !!!!!!!!!!!!!!!!!!!!        

    return flask.render_template(
        'models/texts/generic/new.html',
        extension_id=extension_id,
        extension_title=extensions.data.get_extension(extension_id).get_title() if extension_id else None,
        form=form,
        frameworks=frameworks.get_frameworks(),
        job=job)

@blueprint.route('<extension_id>.json', methods=['POST'])
@blueprint.route('<extension_id>', methods=['POST'], strict_slashes=False)
@blueprint.route('.json', methods=['POST'])
@blueprint.route('', methods=['POST'], strict_slashes=False)
# @utils.auth.requires_login(redirect=False)        #    !!!!!!!!!!!!!!!!!
def create(extension_id=None):
    """
    Create a new GenericTextModelJob
    Returns JSON when requested: {job_id,name,status} or {errors:[]}
    """
    
    form=GenericTextModelForm(request.form)
    
    # Is there a request to clone a job with ?clone=<job_id>
    fill_form_if_cloned(form)
    
#     form_valid = form.validate_on_submit()
#     if not form.validate_on_submit():
#     if not form_valid:

    if not form:        #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if request_wants_json():
            return flask.jsonify({'errors': form.errors}), 400
        else:
            return flask.render_template(
                'models/texts/generic/new.html',
                extension_id=extension_id,
                extension_title=extensions.data.get_extension(extension_id).get_title() if extension_id else None,
                form=form,
                frameworks=frameworks.get_frameworks(),

            ), 400
            
    datasetJob = scheduler.get_job(form.dataset.data)
    if not datasetJob:
        raise werkzeug.exceptions.BadRequest(
            'Unknown dataset job_id "%s"' % form.dataset.data)
        
#     strFeatures=""            
#     for itemList in datasetJob.extension_userdata['train_x_col_tuples']:
#         if str(itemList[1])=="True":
#             strFeatures+=str(itemList[0])+","
#     strFeatures=strFeatures.rstrip(",")
# 
    datasetJob.extension_userdata['predicted_data_file_data']=""
    datasetJob.extension_userdata['predicted_data_file_data_saved']=""     

    config=getConfigparserFromJobAndForm(datasetJob,form)

    print("CONFIG WRTING \n", config)
    
    strJobDir=str(datasetJob._dir)
    strConfigPath=strJobDir+"/"+"config.ini"
          
    with open(strConfigPath, 'w') as configfile:
        config.write(configfile)    
            
#     datasetJob.extension_userdata['config_contents']= config
    
    datasetJob.save()

    n_jobs=1
    gpu_count = 1

    jobs = []
    
    extra = ''

    job = None
    try:
        job = GenericTextModelJob(
            username=utils.auth.get_username(),
            name=form.model_name.data + extra,
            group=form.group_name.data,
            dataset_id=datasetJob.id(),
        )

        fw = frameworks.get_framework_by_id(form.framework.data)

        job.tasks.append(fw.create_train_task(
            job=job,
            dataset=datasetJob,
            gpu_count=gpu_count,
            batch_size=form.batch_size.data[0],
            solver_type=form.solver_type.data,

        )
        )

        # Save form data with the job so we can easily clone it later.
        save_form_to_job(job, form)

        jobs.append(job)
        
        scheduler.running = True    #    !!!!!!!!!!!!!!!!!!!!!!!!
        scheduler.add_job(job)
        
        strErrorMessage=scheduler.main_thread()          #    !!!!!!!!!!!!!!!!!!!!!!!
        if strErrorMessage!="":
            
            scheduler.delete_job(job)
            error_type = "Input data error."
            trace = None
            description = None
            status_code = 500
            return flask.render_template('error.html',
                                         title=error_type,
                                         message=strErrorMessage,
                                         description=description,
                                         trace=trace,
                                         ), status_code
                                     
        if n_jobs == 1:
            if request_wants_json():
                return flask.jsonify(job.json_dict())
            else:
                return flask.redirect(flask.url_for('model.views.show', job_id=job.id()))

    except:
        if job:
            scheduler.delete_job(job)
        raise

    if request_wants_json():
        return flask.jsonify(jobs=[j.json_dict() for j in jobs])

    # If there are multiple jobs launched, go to the home page.
    return flask.redirect('/')


def show(job, related_jobs=None):
    """
    Called from model.views.models_show()
    """
    data_extensions = get_data_extensions()
    view_extensions = get_view_extensions()

    return flask.render_template(
        'models/texts/generic/show.html',
        job=job,
        data_extensions=data_extensions,
        view_extensions=view_extensions,
        related_jobs=related_jobs,
    )


@blueprint.route('/timeline_tracing', methods=['GET'])
def timeline_tracing():
    """
    Shows timeline trace of a model
    """
    job = job_from_request()

    return flask.render_template('models/timeline_tracing.html', job=job)

def get_datasets(extension_id):
    print('&&&&&& GET DATASET ', extension_id)
    tempppp = []
    if extension_id:
        print('possible JOB VALUES \n')
        for j in scheduler.jobs.values():
            #print(j, " j ext id ", j.extension_id, " j.status.is_running() ",j.status.is_running(), " j.status  ", j.status)
            if isinstance(j, GenericDatasetJob) and j.extension_id == extension_id and (j.status.is_running() or j.status == Status.DONE):
                print('ACCEPTED JOB VALUE ', j.id())
                tempppp.append(j.id())


        print("close job values\n\n")
        jobs = [j for j in scheduler.jobs.values()
                if isinstance(j, GenericDatasetJob) and
                j.extension_id == extension_id and (j.status.is_running() or j.status == Status.DONE)]

        print('FINAL ACCEPTED JOB IDS ', tempppp)
    else:
        jobs = [j for j in scheduler.jobs.values()
                if (isinstance(j, GenericDatasetJob)) and
                (j.status.is_running() or j.status == Status.DONE)]
#     return [(j.id(), j.name()) for j in sorted(jobs, key=lambda x, y: cmp(y.id(), x.id()))]
    return [(j.id(), j.name()) for j in sorted(jobs,key=lambda x: x.id())]      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!

#    ??????????????????????????????????????????????????????????????
def fillJobAndFormFromConfigparser(config,listOfColumns,job,form):
    
#     global strTrueFalse
    
    form.batch_size.data[0]=int(config.get('Numbers', 'Training_Batch_Length'))
    form.max_doc_length.data=int(config.get('Numbers', 'Maximum_String_Length'))
    form.word_embed_size.data=int(config.get('Numbers', 'Word_Embedding_Size'))
    form.validation_percents.data=int(config.get('Numbers', 'Validation_Percents'))
    form.number_of_steps.data=int(config.get('Numbers', 'Number_of_Steps'))    

    strFeatures=config.get('Fields', 'Training_Column_Titles')
    listTemp=list(strFeatures.split(","))
    listFEATURE_COLUMNS=[x.strip() for x in listTemp]
    
    if len(listFEATURE_COLUMNS)>0:
        listOfXTuples=[]
        for itemList in listOfColumns:
            if itemList in listFEATURE_COLUMNS:
                myTuple=(str(itemList),"True") 
            else:
                myTuple=(str(itemList),"False")
            listOfXTuples.append(myTuple)
            
    form.select_x_columns.data=listOfXTuples[0][0]      #    Initial column
    form.select_true_or_false.data=listOfXTuples[0][1]  #    Initial column's contents
    form.contents_x_selected.data=listOfXTuples[0][1]  #    Initial column's contents


    form.select_true_or_false.data = "False"  #    Initial column's contents
    form.contents_x_selected.data = "No features added yet. Select from Dropdown"

    print('\nform.select_x_columns.data ', form.select_x_columns.data )
    print('\nform.select_true_or_false.data ', form.select_true_or_false.data )
    print('\nform.contents_x_selected.data ', form.contents_x_selected.data )
    form.selected_y_column.data=config.get('Fields', 'Predicted_Column_Title')
    form.solver_type.data=str(config['Titles']['Prediction_Algorithm_Title'])
    
    #strXCol=listOfXTuples[0][0]
    strXCol = "null"
    #     strTrueFalse=listOfXTuples[0][1]
                    
    print("BEFORE JOB EXTENSION")
    job.extension_userdata['config_contents']= config
    job.extension_userdata['train_x_col_tuples']= listOfXTuples
    job.extension_userdata['train_y_col']= config.get('Fields', 'Predicted_Column_Title')
    print("DONE WITH JOB EXTENSION")
                
    return job,form,strXCol

def getConfigparserFromJobAndForm(job,form):

    config = configparser.ConfigParser()
    
    strFeatures=""            
    for itemList in job.extension_userdata['train_x_col_tuples']:
        if str(itemList[1])=="True":
            strFeatures+=str(itemList[0])+","
          
    strFeatures=strFeatures.rstrip(",")
             
    config['Paths'] = {}
    config['Paths']['Training_Data_Path'] = str(job.extension_userdata['train_data_file_data'])
    config['Paths']['Predicted_Data_Path'] = job.extension_userdata['predicted_data_file_data']
    config['Paths']['Predicted_Data_Path_Saved'] = job.extension_userdata['predicted_data_file_data_saved']        
     
    config['Titles'] = {}
    config['Titles']['Prediction_Algorithm_Title']=str(form.solver_type.data)       
 
    config['Fields'] = {}
    config['Fields']['Training_Column_Titles']=strFeatures
    config['Fields']['Predicted_Column_Title']=job.extension_userdata['train_y_col']
             
    config['Numbers'] = {}
    config['Numbers']['Training_Batch_Length']=str(form.batch_size.data[0])                 
    config['Numbers']['Maximum_String_Length']=str(form.max_doc_length.data)         
    config['Numbers']['Word_Embedding_Size']=str(form.word_embed_size.data) 
    config['Numbers']['Validation_Percents']=str(form.validation_percents.data)
    config['Numbers']['Number_of_Steps']=str(form.number_of_steps.data)            #    !!!!!!!!!!!!!!!!!!!!!!!
    
    config['Booleans'] = {} 
    if form.batch_size.data[0]==0:
        config['Booleans']['Using_Batch_Sampling']="False"
    else:
        config['Booleans']['Using_Batch_Sampling']="True"        
    
    return  config       

def get_standard_networks():
    return [
        ('lenet', 'LeNet'),
        ('alexnet', 'AlexNet'),
        ('googlenet', 'GoogLeNet'),
    ]

def get_default_standard_network():
    return 'alexnet'

@blueprint.route('/new/solver_type/<extension_id>', methods=['GET','POST'])
def solver_type(extension_id=None):
    
    global cur_job_id
    
    strSolverID=get_request_arg('solver_id')
    
    choices = get_datasets(extension_id)
    
#     job_id=choices[0][0]       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#     job = scheduler.get_job(job_id)
    
    job = scheduler.get_job(cur_job_id)    
    job.extension_userdata['solver_type']=strSolverID
    
    job.save()      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    return "OK"            

@blueprint.route('/new/select_x_columns', methods=['GET','POST'])
@blueprint.route('/new/select_x_columns/<extension_id>', methods=['GET','POST'])
def select_x_columns(extension_id=None):

#     global strXCol,strTrueFalse
    global strXCol,cur_job_id

    strXCol = get_request_arg('x_col_id')
    choices = get_datasets(extension_id)
    print('\n\n@@@ new/select_X_cols CHOSE STRXCOL IN VIEWS ' + strXCol)
#     job_id=choices[0][0]       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#     job = scheduler.get_job(job_id)
    
    job = scheduler.get_job(cur_job_id)    

#     if job.form_data['form']:
#         form=job.form_data['form']
#     strTrueFalse=str(form.select_true_or_false.data).capitalize()

#     listOfXTuples=list(job.extension_userdata['train_x_col_tuples'])
#     listTemp=[]
#     for myTuple in listOfXTuples:
#         if str(myTuple[0])==strXCol:
#             myTuple2=(strXCol,strTrueFalse)
#             listTemp.append(myTuple2)
#         else:
#             listTemp.append(myTuple)
            
    listOfXTuples=list(job.extension_userdata['train_x_col_tuples'])

    print('LIST OF TUPLES ' ,listOfXTuples )
    for myTuple in listOfXTuples:

        if str(myTuple[0])==strXCol:
            strTrueFalse=str(myTuple[1])
            break
 
#     job.extension_userdata['train_x_col_tuples']= listTemp
    
    return strTrueFalse 
#     return "OK"

@blueprint.route('/new/select_y_column/<extension_id>', methods=['GET','POST'])
def select_y_column(extension_id=None):
    global strXCol, cur_job_id
    
    choices = get_datasets(extension_id)
    
#     job_id=choices[0][0]       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#     job = scheduler.get_job(job_id)


    job = scheduler.get_job(cur_job_id)    
    
    job.extension_userdata['train_y_col']=strXCol

    print("***** string Y COLUMN ", strXCol)
    job.save()      #    ????????????????????????

               
    return strXCol

@blueprint.route('/new/select_true_or_false/<extension_id>', methods=['GET','POST'])
def select_true_or_false(extension_id=None):
    
#     global strXCol,strTrueFalse
    global strXCol,cur_job_id
    
    strTrueFalse=get_request_arg('tr_fa_id')
    print('***** SELECT TRUE OR FALSE ' , strTrueFalse )
    choices = get_datasets(extension_id)

    
#     job_id=choices[0][0]       #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
#     job = scheduler.get_job(job_id)
    
    job = scheduler.get_job(cur_job_id)    
    
#     if job.form_data['form']:
#         form=job.form_data['form']
#     strXCol=str(form.select_x_columns.data).capitalize()
    
    listOfXTuples=list(job.extension_userdata['train_x_col_tuples'])
    listTemp=[]
    for myTuple in listOfXTuples:
        if str(myTuple[0])==strXCol:
            myTuple2=(strXCol,strTrueFalse)
            listTemp.append(myTuple2)
        else:
            listTemp.append(myTuple)

    job.extension_userdata['train_x_col_tuples']= listTemp
    
    job.save()      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!


    print("\nlistOfXTuples in select_true_false ", listOfXTuples)
    print("\nlistTemp ", listTemp)
    only_true_features = []
    for it in listTemp:
        if it[1] == "True":
            only_true_features.append(it[0])

    print("listof xTuples ", only_true_features)
    return str(only_true_features)
    #return strTrueFalse

def change_form_with_train_params(form,job):

#     global strXCol,strTrueFalse
    global strXCol
       
#     job = scheduler.get_job(job_id)
    
    listOfColumns=list(job.extension_userdata['train_columns_list'])

    listOfColumns=listOfColumns[1:]

    print('list columns in model/texts/generic/views.py ', listOfColumns)


    
#    *********** Fill out list of X_column choices *************
    form.select_x_columns.choices=[]
    listOfXTuples=[]


    for itemList in listOfColumns:
        myTuple=(str(itemList),str(itemList))

        form.select_x_columns.choices.append(myTuple)       #    Fill choices !!!!!!!!!!!!!
        myTuple2=(str(itemList),"False")

        listOfXTuples.append(myTuple2)
    
    strJobDir=str(job._dir)
    strConfigPath=strJobDir+"/"+"config.ini"
    
    config = configparser.ConfigParser()
    
    #if os.path.exists(strConfigPath):
    if 1==0:
        
        config.read(strConfigPath)
        print('*****************CONFIg EXIST '+ strConfigPath)
               
#         form.batch_size.data[0]=int(config.get('Numbers', 'Training_Batch_Length'))
#         form.max_doc_length.data=int(config.get('Numbers', 'Maximum_String_Length'))
#         form.word_embed_size.data=int(config.get('Numbers', 'Word_Embedding_Size'))
#         form.validation_percents.data=int(config.get('Numbers', 'Validation_Percents'))
# 
#         strFeatures=config.get('Fields', 'Training_Column_Titles')
#         listTemp=list(strFeatures.split(","))
#         listFEATURE_COLUMNS=[x.strip() for x in listTemp]
#         
#         if len(listFEATURE_COLUMNS)>0:
#             listOfXTuples=[]
#             for itemList in listOfColumns:
#                 if itemList in listFEATURE_COLUMNS:
#                     myTuple=(str(itemList),"True") 
#                 else:
#                     myTuple=(str(itemList),"False")
#                 listOfXTuples.append(myTuple)

        job,form,strXCol=fillJobAndFormFromConfigparser(config,listOfColumns,job,form)
        print("return from filljon")
        print("*** FORM\n " ,form)

    if 1==1:
        print("config doesnt exist")
        config['Paths'] = {}
        config['Paths']['Training_Data_Path'] = str(job.extension_userdata['train_data_file_data'])
        config['Paths']['Predicted_Data_Path'] = ""
        config['Paths']['Predicted_Data_Path_Saved'] = ""        
        
        config['Titles'] = {}
        config['Titles']['Prediction_Algorithm_Title']='Bag-of-words'        

        config['Fields'] = {}
        config['Fields']['Training_Column_Titles']=""
        config['Fields']['Predicted_Column_Title']=""
                
        config['Numbers'] = {}
        config['Numbers']['Training_Batch_Length']=str(form.batch_size.data[0])
        config['Numbers']['Maximum_String_Length']=str(form.max_doc_length.data)         
        config['Numbers']['Word_Embedding_Size']=str(form.word_embed_size.data) 
        config['Numbers']['Validation_Percents']=str(form.validation_percents.data)
        config['Numbers']['Number_of_Steps']=str(form.number_of_steps.data)         
        
        listOfXTuples=[]
        for itemList in listOfColumns:
            myTuple=(str(itemList),"False")
            listOfXTuples.append(myTuple)                            
        
        with open(strConfigPath, 'w') as configfile:
            config.write(configfile)

        print('\n\n CONFIG EXISTS !!!!!!!')
        form.select_x_columns.data=listOfXTuples[0][0]      #    Initial column
        #form.select_true_or_false.data=listOfXTuples[0][1]  #    Initial column's contents
        #form.contents_x_selected.data=listOfXTuples[0][1]  #    Initial column's contents
        form.select_true_or_false.data = "False"  # Initial column's contents
        form.contents_x_selected.data = "No features added yet. Select from Dropdown"

        print('\nPREDICTED COLUMN TITLE  ', config.get('Fields', 'Predicted_Column_Title'))
        
        form.selected_y_column.data=config.get('Fields', 'Predicted_Column_Title')
        form.solver_type.data=str(config['Titles']['Prediction_Algorithm_Title'])
        
        strXCol=listOfXTuples[0][0]
    #     strTrueFalse=listOfXTuples[0][1]
                        
        job.extension_userdata['config_contents']= config
        job.extension_userdata['train_x_col_tuples']= listOfXTuples
        job.extension_userdata['train_y_col']= config.get('Fields', 'Predicted_Column_Title')
                   
    return form,job

def get_data_extensions():
    """
    return all enabled data extensions
    """
    data_extensions = {"all-default": "Default"}
    all_extensions = extensions.data.get_extensions()
    for extension in all_extensions:
        data_extensions[extension.get_id()] = extension.get_title()
    return data_extensions


def get_view_extensions():
    """
    return all enabled view extensions
    """
    view_extensions = {}
    all_extensions = extensions.view.get_extensions()
    for extension in all_extensions:
        view_extensions[extension.get_id()] = extension.get_title()
    return view_extensions
