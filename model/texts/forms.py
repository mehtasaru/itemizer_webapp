# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from wtforms import validators

from ..forms import ModelForm
import utils


class TextModelForm(ModelForm):
    """
    Defines the form used to create a new TextModelJob
    """
    print("in TextModelForm in forms.py")
    
#    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    select_x_columnstemp = utils.forms.SelectField(
        'Select columns for X/Y',
        choices=[
            ('description', 'description'),
            ('manufacturer', 'manufacturer'),
            ('supplier', 'supplier'),
        ],

        default='description',
        tooltip="Select columns to be used as independent (X) and dependent (Y) variables."
    )
    select_x_columns = utils.forms.SelectField(
        'Select columns for X/Y',
        choices=[
            ('description', 'description'),
            ('manufacturer', 'manufacturer'),
            ('supplier', 'supplier'),
        ],
        default='description',
        tooltip="Select columns to be used as independent (X) and dependent (Y) variables."
    )
    select_true_or_false = utils.forms.SelectField(
        'Select True/False',
        choices=[
            ("True", 'True'),
            ("False", 'False'),
        ],

        default="False",
        tooltip="Select True/False for column to be used for training."
    )
    
    contents_x_selected = utils.forms.TextAreaField(
        'Chosen Features',
        id='contents_x_selected',
        tooltip="True/False of selected X column for training."
    )       
    
    selected_y_column = utils.forms.TextAreaField(
        'Selected Y-column',
        id='selected_y_column',
        tooltip="Selected Y column for training."
    )        
#    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
