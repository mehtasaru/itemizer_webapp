# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from .classification import TextClassificationModelJob
from .generic import GenericTextModelJob
from .job import TextModelJob

__all__ = [
    'TextClassificationModelJob',
    'GenericTextModelJob',
    'TextModelJob',
]
