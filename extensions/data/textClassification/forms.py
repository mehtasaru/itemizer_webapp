# Copyright (c) 2016-2017, NVIDIA CORPORATION.  All rights reserved.
from __future__ import absolute_import

import os

import utils
from utils import subclass
# from flask.ext.wtf import Form
from flask_wtf import Form      #    !!!!!!!!!!!!!!!!!!
from wtforms import validators

from utils.forms import validate_required_iff, validate_greater_than

def validate_file_path(form, field):
    if not field.data:
        pass
    else:
        # make sure the filesystem path exists
        if not os.path.exists(field.data) and not os.path.isdir(field.data):
            raise validators.ValidationError(
                'File does not exist or is not reachable')
        else:
            return True


@subclass
class DatasetForm(Form):
    """
    A form used to create a text classification dataset
    """
    
    train_data_file = utils.forms.FileField(
        u'Training data (.xlsx)',
        validators=[
            validate_required_iff(method='textfile',
                                  textfile_use_local_files=False)
        ]
    )
    



