# Copyright (c) 2016-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import copy
from pkg_resources import iter_entry_points

# Entry point group (this is the key we use to register and
# find installed plug-ins)
GROUP = "plugins.view"

# def get_default_extension():
#     """
#     return the default view extension
#     """
#     return rawData.Visualization


def get_extensions():
    """
    return set of data data extensions
    """
#     extensions = copy.copy(builtin_view_extensions)
    extensions=[]
    # find installed extension plug-ins
    for entry_point in iter_entry_points(group=GROUP, name=None):
        extensions.append(entry_point.load())

    return extensions


def get_extension(extension_id):
    """
    return extension associated with specified extension_id
    """
    for extension in get_extensions():
        if extension.get_id() == extension_id:
            return extension
    return None
