# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019


# #    ?????????????????????????????????
# from gevent import monkey
# import threading
# with threading.RLock():
#     monkey.patch_all()
# #    ???????????????????????????????????

import argparse
import os.path
import sys

PARENT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
found_parent_dir = False
for p in sys.path:
    if os.path.abspath(p) == PARENT_DIR:
        found_parent_dir = True
        break
if not found_parent_dir:
    sys.path.insert(0, PARENT_DIR)

def main():
    parser = argparse.ArgumentParser(description='Itemizer server')
    parser.add_argument(
        '-p', '--port',
        type=int,
        default=5000,
#         default=5001,       #    !!!!!!!!!!!!!!!!!!!!!!!!!
        help='Port to run app on (default 5000)'
    )
    parser.add_argument(
        '-d', '--debug',
        action='store_true',
        help=('Run the application in debug mode (reloads when the source '
              'changes and gives more detailed error messages)')
    )
    parser.add_argument(
        '--version',
        action='store_true',
        help='Print the version number and exit'
    )

    args = vars(parser.parse_args())
    
    import webapp    
    
    #args['debug']=True      #    !!!!!!!!!!!!!!!

    try:
        if not webapp.scheduler.start():
            print ('ERROR: Scheduler would not start')
        else:
            webapp.app.debug = args['debug']
            webapp.socketio.run(webapp.app, '0.0.0.0', args['port'])
                        
    except KeyboardInterrupt:
        pass
    finally:
        webapp.scheduler.stop()


if __name__ == '__main__':
    main()
