#!/usr/bin/env python2
# Copyright (c) 2016-2017, NVIDIA CORPORATION.  All rights reserved.

import argparse
# Find the best implementation available
# try:
#     from cStringIO import StringIO
# except ImportError:
#     from StringIO import StringIO
from io import StringIO     #    !!!!!!!!!!!!!!!!!!!!!!!
    
import lmdb
import logging
import numpy as np
import os

# import Queue
from multiprocessing import Queue   #    !!!!!!!!!!!!!!!!!!!
import sys
# import threading    #    !!!!!!!!!!!!!!!!!!!!!!!!

sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))))
import config  # noqa
import extensions   #, log  # noqa
from job import Job  # noqa

import copy     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
import pandas as pd     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!

#    ****************** ML processing class *******************
class MLSituation():
    def __init__(self):
        
        self.data = None          # dataframe
        self.trainingData = None
        self.testingData = None
        self.verifyData=None
        
    def cleanupString(self,strDescription):
        
        if strDescription=='_NA':
            return strDescription
        
        strDescription=strDescription.upper()
     
    #  Discard leading special symbols
        lenStrDescr=len(strDescription)        
        j=0
        while not strDescription[j].isalpha() and not strDescription[j].isdigit() and j<lenStrDescr-1:     #   Discard first non-letters
            j+=1
        if j>0:
            strDescription=strDescription[j:]
             
        lenStrDescr=len(strDescription)
        for j in range(lenStrDescr):
            if strDescription[j].isalpha():     #   Continue till end of letters
                continue
            if strDescription[j]==',':       #    Replace first comma by space
                if j==0:
                    strDescription=strDescription[1:]
                else:
                    strDescription=strDescription[:j]+' '+strDescription[j+1:]
                    
        strDescription=strDescription.strip('\n')
                    
        return strDescription
      
    def cleanupColumnsDS(self,DataFrameIn,featureNames,label):

        featureNamesExt = copy.copy(list(featureNames))
        featureNamesExt.append(label)

#         DataFrameOut=DataFrameIn.copy(deep=True)
        listFrameOut=[]
        listIndexDescrCol=[]
        for itemList in featureNamesExt:
            indexDescrCol=DataFrameIn.columns.get_loc(str(itemList))            
            listIndexDescrCol.append(indexDescrCol)
            
        lenDataFrame=len(DataFrameIn)
        
        for i in range(lenDataFrame):
            mlsRecord=DataFrameIn.iloc[i,:].values
            boolOK=True
            for indexDescrCol in listIndexDescrCol:
                
                try:
                    objField=mlsRecord[int(indexDescrCol)]
                    if isinstance(objField,str):
                        strField=str(mlsRecord[int(indexDescrCol)]).upper().strip()
                    else:
                        strField="_NA"                   
                    if strField=="":
                        strField="_NA"
                      
                except (RuntimeError, TypeError, NameError):
                    strField="_NA" 
                    
                if strField!="_NA": 
                    strField=self.cleanupString(strField)
                    
                mlsRecord[int(indexDescrCol)]=strField
                
            listFrameOut.append(mlsRecord)
                
        DataFrameOut=pd.DataFrame(listFrameOut,columns=DataFrameIn.columns)
        
        return DataFrameOut     
        
    def separateTrainAndTestRecordsDS(self, test_fraction):

        self.train_testData=self.data

        self.train_testData["test"] = self.train_testData.apply(lambda x: 1 if np.random.rand() < test_fraction else 0, axis=1)
        self.trainingData = self.train_testData[self.train_testData["test"] == 0].copy(deep=True)
        self.testingData = self.train_testData[self.train_testData["test"] == 1].copy(deep=True)

    def setX_featuresDS(self,DataFrameIn, featureNames):
 
        DataFrameOut=DataFrameIn.copy(deep=True)
        DataFrameOut["X"]=""
        
        listOfNewColumns=DataFrameOut.columns
       
        lenDataFrame=len(DataFrameOut)
        indexXCol=DataFrameOut.columns.get_loc("X")             
        listIndexDescrCol=[]
        for itemList in featureNames:
            indexDescrCol=int(DataFrameOut.columns.get_loc(str(itemList)))            
            listIndexDescrCol.append(indexDescrCol)
            
        listIndexDescrCol.sort()   
        listFrameOut=[]        
        for i in range(lenDataFrame):
            mlsRecord=DataFrameOut.iloc[i,:].values
            boolOK=True
            strSum=""
            for indexDescrCol in listIndexDescrCol:
                 
                strField=str(mlsRecord[indexDescrCol])
                if strField=="_NA":
                    boolOK=False                   
                    break
                     
                strSum+=strField+" "
                 
            if boolOK:
                mlsRecord[indexXCol]=strSum
                listFrameOut.append(mlsRecord)                
            else:
                continue

        DataFrameOut=pd.DataFrame(listFrameOut,columns=listOfNewColumns)
        
        return DataFrameOut

class DbCreatorIF(object):
    
    def __init__(self, strDATASET_PATH):
        
        self.strDATASET_PATH = strDATASET_PATH
        self.strDATA_FOLDER=os.path.dirname(self.strDATASET_PATH)
        if self.strDATA_FOLDER!="":
            self.strDATA_FOLDER+='/'  
        strBaseName=os.path.basename(self.strDATASET_PATH)
        listBaseName=list(strBaseName.split('.'))
        self.strFILE_NAME=str(listBaseName[0]).strip()
        
        self.mls = MLSituation() 
        
#  ********** Load training data ****************
    def loadTrainingDataIF(self):
        
        strErrorMessage=""
                
        strDataPathInit=self.strDATASET_PATH
        if not os.path.exists(strDataPathInit):
            strErrorMessage='Error: File: '+strDataPathInit+' does not exist.'
            return strErrorMessage
        
        strDataPathPkl=self.strDATA_FOLDER+self.strFILE_NAME+'_DF.pkl'
        self.listOfDFColumns=[]

        if not os.path.exists(strDataPathPkl):

            self.mls.data = pd.read_excel(strDataPathInit, encoding="ISO-8859-1", keep_default_na=False)
           
            listTemp=list(self.mls.data.columns)            
            self.listOfDFColumns=[x.lower().strip().replace(" ", "_") for x in listTemp]            
            
#   ******* Pickle initial dataframe *********** 
            self.mls.data.columns=self.listOfDFColumns
            self.mls.data.to_pickle(strDataPathPkl)
            
        else:
          
            self.mls.data = pd.read_pickle(strDataPathPkl)
            self.listOfDFColumns = copy.copy(list(self.mls.data.columns))

        listTemp=copy.copy(list(self.mls.data.columns))          
        self.listOfDFColumns=["#"]
        self.listOfDFColumns.extend(listTemp)

        self.listOfLists=[]            
        for i in range(len(self.mls.data)):
            mlsRecord=self.mls.data.iloc[i,:].values
            listTemp2=mlsRecord.tolist()
            listTemp=[str(i+1)]
            listTemp.extend(listTemp2)            
            self.listOfLists.append(listTemp)
            
        return strErrorMessage,self.listOfLists,self.listOfDFColumns

def create_generic_db(jobs_dir, dataset_id, stage):
    """
    Create a generic DB
    """

    indEqual=str(jobs_dir).find("=")
    jobs_dir=jobs_dir[indEqual+1:]

    if len(stage)>0:    
        indEqual=str(stage).find("=")
        stage=stage[indEqual+1:]    
    
    # job directory defaults to that defined in config
    if jobs_dir == 'none':
        jobs_dir = config.config_value('jobs_dir')

    # load dataset job
    dataset_dir = os.path.join(jobs_dir, dataset_id)

    if not os.path.isdir(dataset_dir):
        raise IOError("Dataset dir %s does not exist" % dataset_dir)
    dataset = Job.load(dataset_dir)

    # create instance of extension
    extension_id = dataset.extension_id
    extension_class = extensions.data.get_extension(extension_id)
    extension = extension_class(**dataset.extension_userdata)

    strDATASET_PATH=extension.train_data_file_data
    db_creator = DbCreatorIF(strDATASET_PATH)
    
    strErrorMessage,listOfLists,listOfDFColumns=db_creator.loadTrainingDataIF()
    if strErrorMessage!="":
        raise ValueError('Loading raw data failed (%s)!' % (strErrorMessage))
    
#     dataset.extension_userdata['train_data_list']=listOfLists       #    !!!!!!!!!!!!!!!!!!!!!!!!
#     dataset.extension_userdata['train_columns_list']=listOfDFColumns       #    !!!!!!!!!!!!!!!!!!!!!!!!    
    
    return listOfLists,listOfDFColumns
