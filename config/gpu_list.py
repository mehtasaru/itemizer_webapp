# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.

from __future__ import absolute_import

import sys
sys.path.append("..") # Adds higher directory to python modules path.

from . import option_list
import device_query


option_list['gpu_list'] = ','.join([str(x) for x in range(len(device_query.get_devices()))])     #    !!!!!!!!!!!!!!