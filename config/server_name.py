# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os
import platform

from . import option_list

if 'ITEMIZER_SERVER_NAME' in os.environ:
    value = os.environ['ITEMIZER_SERVER_NAME']
else:
    value = platform.node()

option_list['server_name'] = value
