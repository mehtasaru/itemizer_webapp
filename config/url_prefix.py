# Copyright (c) 2016, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os

from . import option_list

option_list['url_prefix'] = os.environ.get('ITEMIZER_URL_PREFIX', '')
