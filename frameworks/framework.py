# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

# from inference.tasks import InferenceTask

class Framework(object):

    """
    Defines required methods to interact with a framework
    """

    def get_name(self):
        """
        return self-descriptive name
        """
        return self.NAME

    def get_id(self):
        """
        return unique id of framework instance
        """
        return self.framework_id

    def supports_timeline_traces(self):
        """
        return whether framework supports creating timeline traces
        """
        return self.SUPPORTS_TIMELINE_TRACING

    def supports_solver_type(self, solver_type):
        """
        return whether framework supports this solver_type
        """
        if not hasattr(self, 'SUPPORTED_SOLVER_TYPES'):
            raise NotImplementedError
        assert isinstance(self.SUPPORTED_SOLVER_TYPES, list)
        return solver_type in self.SUPPORTED_SOLVER_TYPES

    def validate_network(self, data):
        """
        validate a network (must be implemented in child class)
        """
        raise NotImplementedError('Please implement me')

    def create_train_task(self, **kwargs):
        """
        create train task
        """
        raise NotImplementedError('Please implement me')



