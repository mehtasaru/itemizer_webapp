# Copyright (c) 2015-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from .framework import Framework
from config import config_value

__all__ = [
    'Framework',
]

if config_value('tensorflow')['enabled']:
    from .tensorflow_framework import TensorflowFramework
    __all__.append('TensorflowFramework')

#
#  create framework instances
#
tensorflow = TensorflowFramework() if config_value('tensorflow')['enabled'] else None

#
#  utility functions
#
def get_frameworks():
    """
    return list of all available framework instances
    there may be more than one instance per framework class
    """
    frameworks = []         #    !!!!!!!!!!!!!!!!!!
    if tensorflow:
        frameworks.append(tensorflow)
    return frameworks


def get_framework_by_id(framework_id):
    """
    return framework instance associated with given id
    """
    for fw in get_frameworks():
        if fw.get_id() == framework_id:
            return fw
    return None
