# Copyright (c) 2016, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import os
import re
import subprocess
import tempfile
import sys

from .errors import NetworkVisualizationError
from .framework import Framework
import utils
from model.tasks import TensorflowTrainTask
from utils import subclass, override, constants


@subclass
class TensorflowFramework(Framework):
    """
    Defines required methods to interact with the Tensorflow framework
    """

    # short descriptive name
    NAME = 'Tensorflow'

    # identifier of framework class
    CLASS = 'tensorflow'

    # whether this framework can shuffle data during training
    CAN_SHUFFLE_DATA = True
    SUPPORTS_PYTHON_LAYERS_FILE = False
    SUPPORTS_TIMELINE_TRACING = True

    SUPPORTED_SOLVER_TYPES = ['Bag-of-words','Recurrent_NN','Multinomial_NB']       #    !!!!!!!!!!!!!!!!!!!!!!

    SUPPORTED_DATA_TRANSFORMATION_TYPES = ['SELECT_X_COLUMNS','SELECT_TRUE_OR_FALSE','SELECTED_Y_COLUMN','CONTENTS_X_SELECTED','PRETRAINED_NETWORK']     #    !!!!!!!!!!!!!!!!!!!!!!!!!!
    
    def __init__(self):
        super(TensorflowFramework, self).__init__()
        # id must be unique
        self.framework_id = self.CLASS

    @override
    def create_train_task(self, **kwargs):
        """
        create train task
        """
        return TensorflowTrainTask(framework_id=self.framework_id, **kwargs)

    @override
    def validate_network(self, data):
        """
        validate a network
        """
        return True


