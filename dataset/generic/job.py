# Copyright (c) 2016-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from ..job import DatasetJob
from dataset import tasks
from utils import subclass, override, constants

# NOTE: Increment this every time the pickled object changes
PICKLE_VERSION = 1

#    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
backend='XLSX'
num_threads=1
force_same_shape=1
batch_size=32
#    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

@subclass
class GenericDatasetJob(DatasetJob):
    """
    A Job that creates a dataset using a user-defined extension
    """
    
    def __init__(self,
   
                 extension_id,
                 extension_userdata,
                 **kwargs
                 ):
        self.backend = backend

        self.num_threads = num_threads
        self.force_same_shape = force_same_shape
        self.batch_size = batch_size
        self.extension_id = extension_id
        self.extension_userdata = extension_userdata
        
        strFileName=extension_userdata['train_data_file'].filename
        strFileName=strFileName.replace(" ", "_")       #    !!!!!!!!!!!!!!!!!!!!!!!
             
        del self.extension_userdata['train_data_file']
        self.extension_userdata['train_data_file.data'] = strFileName 
        
        super(GenericDatasetJob, self).__init__(**kwargs)
        self.pickver_job_dataset_extension = PICKLE_VERSION
        
        self.extension_userdata['train_data_file_data'] = self._dir+"/"+strFileName

        # create tasks
#         for stage in [constants.TRAIN_DB, constants.VAL_DB, constants.TEST_DB]:
        for stage in [constants.TRAIN_DB]:              #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            self.tasks.append(tasks.CreateGenericDbTask(
                job_dir=self.dir(),
                job=self,
                backend=self.backend,
                stage=stage,
            )
            )

    def __setstate__(self, state):
        super(GenericDatasetJob, self).__setstate__(state)
        self.pickver_job_dataset_extension = PICKLE_VERSION

    def create_db_task(self, stage):
        for t in self.tasks:
            if t.stage == stage:
                return t
        return None

    def create_db_tasks(self):
        return self.tasks

    def get_file_name(self):
        """
        Return file name
        """
        return self.extension_userdata['train_data_file.data']
    
    def get_lines_length(self):
        
        return str(len(self.extension_userdata['train_data_list']))    

    def get_x_col_true_false(self,strXCol):
        """
        Return True/False for current column
        """
        
        listOfXTuples=self.extension_userdata['train_x_col_tuples']
        for itemList in listOfXTuples:
            if itemList[0]==strXCol:
                strTrueFalse=itemList[1]
                break

        return strTrueFalse
 
    @override
    def get_entry_count(self, stage):
        """
        Return the number of entries in the DB matching the specified stage
        """
        return self.create_db_task(stage).entry_count

    @override
    def get_feature_dims(self):
        """
        Return the shape of the feature N-D array
        """
        shape = self.create_db_task(constants.TRAIN_DB).feature_shape
        if len(shape) == 3:
            # assume text and convert CHW => HWC (numpy default for texts)
            shape = [shape[1], shape[2], shape[0]]
        return shape

    @override
    def job_type(self):
        return 'Generic Dataset'

    @override
    def json_dict(self, verbose=False):
        d = super(GenericDatasetJob, self).json_dict(verbose)
        if verbose:
            d.update({
                'create_db_tasks': [{
                    "name": t.name(),
                    "stage": t.stage,
                    "entry_count": t.entry_count,
                    "feature_db_path": t.dbs['features'],
                    "label_db_path": t.dbs['labels'],
                } for t in self.create_db_tasks()],
                'feature_dims': self.get_feature_dims()})
        return d
