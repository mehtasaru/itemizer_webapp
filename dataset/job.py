# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

from job import Job
from utils import subclass

# NOTE: Increment this every time the pickled object changes
PICKLE_VERSION = 1

@subclass
class DatasetJob(Job):
    """
    A Job that creates a dataset
    """

    def __init__(self, **kwargs):
        """
        """
        super(DatasetJob, self).__init__(**kwargs)
        self.pickver_job_dataset = PICKLE_VERSION

    def get_entry_count(self, stage):
        """
        Return the number of entries in the DB matching the specified stage
        """
        raise NotImplementedError('Please implement me')

    def get_feature_dims(self):
        """
        Return the shape of the feature N-D array
        """
        raise NotImplementedError('Please implement me')

