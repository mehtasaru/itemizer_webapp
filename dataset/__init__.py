# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
from __future__ import absolute_import

from .generic import GenericDatasetJob
from .job import DatasetJob

__all__ = [
    'GenericDatasetJob',
    'DatasetJob',
]
