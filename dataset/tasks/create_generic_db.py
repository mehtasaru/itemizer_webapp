# Copyright (c) 2016-2017, NVIDIA CORPORATION.  All rights reserved.
from __future__ import absolute_import

import os
import re
import sys

from task import Task
from utils import subclass, override

# NOTE: Increment this every time the pickled version changes
PICKLE_VERSION = 1

@subclass
class CreateGenericDbTask(Task):
    """
    A task to create a db using a user-defined extension
    """

    def __init__(self, job, backend, stage, **kwargs):
        """
        Arguments:
        """
        self.job = job
        self.backend = backend
        self.stage = stage
#         self.create_db_log_file = "create_%s_db.log" % stage
        self.dbs = {'features': None, 'labels': None}
        self.entry_count = 0
        self.feature_shape = None
        self.label_shape = None
        self.mean_file = None
        super(CreateGenericDbTask, self).__init__(**kwargs)
        self.pickver_task_create_generic_db = PICKLE_VERSION

    @override
    def name(self):
        return 'Create %s DB' % self.stage

    @override
    def __getstate__(self):
        state = super(CreateGenericDbTask, self).__getstate__()
        if 'create_db_log' in state:
            # don't save file handle
            del state['create_db_log']
        return state

    @override
    def __setstate__(self, state):
        super(CreateGenericDbTask, self).__setstate__(state)
        self.pickver_task_create_generic_db = PICKLE_VERSION

    @override
    def before_run(self):
        super(CreateGenericDbTask, self).before_run()
        # create log file
#         self.create_db_log = open(self.path(self.create_db_log_file), 'a')
        # save job before spawning sub-process
        self.job.save()



    @override
    def after_run(self):
        super(CreateGenericDbTask, self).after_run()
#         self.create_db_log.close()

    @override
    def offer_resources(self, resources):
        reserved_resources = {}
        # we need one CPU resource from create_db_task_pool
        cpu_key = 'create_db_task_pool'
        if cpu_key not in resources:
            return None
        for resource in resources[cpu_key]:
            if resource.remaining() >= 1:
                reserved_resources[cpu_key] = [(resource.identifier, 1)]
                return reserved_resources
        return None

