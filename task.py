# Copyright (c) 2014-2017, NVIDIA CORPORATION.  All rights reserved.
#    Changed by LevaData 2019

from __future__ import absolute_import

import logging
import os.path
import platform
import re
# import signal       #    !!!!!!!!!!!!!!!!!!!!
import subprocess
import time

import pandas as pd     #    !!!!!!!!!!!!!!!!!!!!
import copy         #    !!!!!!!!!!!!!!!!!!!!

import flask
import gevent.event

import utils
from config import config_value
from status import Status, StatusCls
# import log

# NOTE: Increment this every time the pickled version changes
PICKLE_VERSION = 1

from tools.create_generic_db import create_generic_db      #    !!!!!!!!!!!!!!!!!!!

import configparser     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!
import shutil,glob           #    !!!!!!!!!!!!!!!!!!!!!!!!!!!

class Task(StatusCls):
    """
    Base class for Tasks
    A Task is a compute-heavy operation that runs in a separate executable
    Communication is done by processing the stdout of the executable
    """

    def __init__(self, job_dir, parents=None):
        super(Task, self).__init__()
        self.pickver_task = PICKLE_VERSION

        self.job_dir = job_dir
        self.job_id = os.path.basename(job_dir)
        
        if parents is None:
            self.parents = None
        elif isinstance(parents, (list, tuple)):
            self.parents = parents
        elif isinstance(parents, Task):
            self.parents = [parents]
        else:
            raise TypeError('parents is %s' % type(parents))

        self.exception = None
        self.traceback = None
#         self.aborted = gevent.event.Event()        #    !!!!!!!!!!!!!!!!!!
        self.aborted = False        #    !!!!!!!!!!!!!!!!!!

#         self.set_logger()
        self.p = None  # Subprocess object for training

    def __getstate__(self):
        d = self.__dict__.copy()

        if 'aborted' in d:
            del d['aborted']
        if 'logger' in d:
            del d['logger']
        if 'p' in d:
            # Subprocess object for training is not pickleable
            del d['p']

        return d

    def __setstate__(self, state):
        self.__dict__ = state

#         self.aborted = gevent.event.Event()    #    !!!!!!!!!!!!!!!!!!!!!!!
#         self.set_logger()

#     def set_logger(self):
#         self.logger = log.JobIdLoggerAdapter(
#             logging.getLogger('webapp'),
#             {'job_id': self.job_id},
#         )

    def name(self):
        """
        Returns a string
        """
        raise NotImplementedError

    def html_id(self):
        """
        Returns a string
        """
        return 'task-%s' % id(self)

    def on_status_update(self):
        """
        Called when StatusCls.status.setter is used
        """
        from webapp import app, socketio

        # Send socketio updates
        message = {
            'task': self.html_id(),
            'update': 'status',
            'status': self.status.name,
            'css': self.status.css,
            'show': (self.status in [Status.RUN, Status.ERROR]),
            'running': self.status.is_running(),
        }
        with app.app_context():
            message['html'] = flask.render_template('status_updates.html',
                                                    updates=self.status_history,
                                                    exception=self.exception,
                                                    traceback=self.traceback,
                                                    )

        socketio.emit('task update',
                      message,
                      namespace='/jobs',
                      room=self.job_id,
                      )

        from webapp import scheduler
        job = scheduler.get_job(self.job_id)
        if job:
            job.on_status_update()

    def path(self, filename, relative=False):
        """
        Returns a path to the given file

        Arguments:
        filename -- the requested file

        Keyword arguments:
        relative -- If False, return an absolute path to the file
                    If True, return a path relative to the jobs directory
        """
        if not filename:
            return None
        if os.path.isabs(filename):
            path = filename
        else:
            path = os.path.join(self.job_dir, filename)
            if relative:
                path = os.path.relpath(path, config_value('jobs_dir'))
        return str(path).replace("\\", "/")

    def ready_to_queue(self):
        """
        Returns True if all parents are done
        """
        if not self.parents:
            return True
        for parent in self.parents:
            if parent.status != Status.DONE:
                return False
        return True

    def offer_resources(self, resources):
        """
        Check the available resources and return a set of requested resources

        Arguments:
        resources -- a copy of scheduler.resources
        """
        raise NotImplementedError

    def before_run(self):
        """
        Called before run() executes
        Raises exceptions
        """
        pass

    def run(self, resources):
        """
        Execute the task

        Arguments:
        resources -- the resources assigned by the scheduler for this task
        """
        
        strErrorMessage=""
        
        self.before_run()

        env = os.environ.copy()
        
        self.status = Status.RUN        #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        import sys
        env['PYTHONPATH'] = os.pathsep.join(['.', self.job_dir, env.get('PYTHONPATH', '')] + sys.path)

#         # https://docs.python.org/2/library/subprocess.html#converting-argument-sequence
#         if platform.system() == 'Windows':
#             args = ' '.join(args)
# #             self.logger.info('Task subprocess args: "{}"'.format(args))    #    !!!!!!!!!!!!!!!!!!!!!!!!!
#         else:
# #             self.logger.info('Task subprocess args: "%s"' % ' '.join(args))    #    !!!!!!!!!!!!!!!!!!!!!!!!!!
#             pass

#    ????????????????????????????????????????????????????????????????
#         self.p = subprocess.Popen(args,
#                                   stdout=subprocess.PIPE,
#                                   stderr=subprocess.STDOUT,
#                                   cwd=self.job_dir,
#                                   close_fds=False if platform.system() == 'Windows' else True,
#                                   env=env,
#                                   )
#    ???????????????????????????????????????????????????????????????????????

#         if isinstance(self, CreateGenericDbTask):

        if self.stage=="train_db":

            jobs_dir=os.path.dirname(self.job_dir)
            dataset_id=self.job_id
                        
            listOfLists,listOfDFColumns=create_generic_db(jobs_dir,dataset_id,"")      #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!     
            
            self.job.extension_userdata['train_data_list']=listOfLists       #    !!!!!!!!!!!!!!!!!!!!!!!!
            self.job.extension_userdata['train_columns_list']=listOfDFColumns       #    !!!!!!!!!!!!!!!!!!!!!!!!
            
            listOfDFColumns=listOfDFColumns[1:]
            listOfXTuples=[]
            for itemList in listOfDFColumns:
                myTuple=(str(itemList),"False")
                listOfXTuples.append(myTuple)             
            
            self.job.extension_userdata['train_x_col_tuples']=listOfXTuples
            
            self.job.save()     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
        elif  self.stage=="training":
             
            strDatasetJobDir=str(self.dataset._dir)
            strConfigPath=strDatasetJobDir+"/"+"config.ini"
            
            strJobDir=str(self.job._dir)
            strConfigPathOut=strJobDir+"/"+"config.ini" 
            
            config = configparser.ConfigParser()        
            config.read(strConfigPath)
            
            strDATASET_PATH = config.get('Paths', 'Training_Data_Path')
            strDATA_FOLDER=os.path.dirname(strDATASET_PATH)
            if strDATA_FOLDER!="":
                strDATA_FOLDER+='/'  
            strBaseName=os.path.basename(strDATASET_PATH)
            listBaseName=list(strBaseName.split('.'))
            strFILE_NAME=str(listBaseName[0]).strip()
            strDataPathPkl=strDATA_FOLDER+strFILE_NAME+'_DF.pkl'            
            
#    ********* Copy training file ***************
            strDATA_FOLDER_NEW=strJobDir+"/"
            strDATASET_PATH_NEW=strDATA_FOLDER_NEW+strBaseName
            shutil.copyfile(strDATASET_PATH, strDATASET_PATH_NEW)

#    ************* Copy support pickle file **********************
            strDataPathPklNew=strDATA_FOLDER_NEW+strFILE_NAME+'_DF.pkl' 
            shutil.copyfile(strDataPathPkl, strDataPathPklNew)
            
            config['Paths']['training_data_path']=strDATASET_PATH_NEW                       
            with open(strConfigPathOut, 'w') as configfile:
                config.write(configfile)                          
    
#    ************* Run Itemizer training *********************
            strErrorMessage,dictResults=self.runItemizer(strConfigPathOut,self.stage)     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                self.status = Status.ABORT
                return strErrorMessage
            
            self.job.dict_train_results=dictResults
            
            self.job.save()     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#             strClassifierPathPklDest=os.path.join(strDATA_FOLDER_NEW,strFILE_NAME+'_Clf_VPr_LCl.pkl')
#             model_dir_dest=os.path.join(strDATA_FOLDER_NEW,strFILE_NAME,"ClfModel")                          
#             
#             dictResults['classifier_pickle_path']=strClassifierPathPklDest
#             dictResults['model_directory']=model_dir_dest              
            
        else:
            
            strJobDir=str(self.job.dict_pretrained["job_dir"])
            strConfigPath=strJobDir+"/"+"config.ini"
            
            config = configparser.ConfigParser()        
            strConfigPath=strJobDir+"/"+"config.ini"
            config.read(strConfigPath)
            config['Paths']['Predicted_Data_Path'] = str(self.dataset.extension_userdata['train_data_file_data'])
            
            strDATASET_PATH=config['Paths']['Predicted_Data_Path']
            strDATA_FOLDER=os.path.dirname(strDATASET_PATH)
            if strDATA_FOLDER!="":
                strDATA_FOLDER+='/'
            strBaseName=os.path.basename(strDATASET_PATH)
            listBaseName=list(strBaseName.split('.'))
            strFILE_NAME=str(listBaseName[0]).strip()
            config['Paths']['Predicted_Data_Path_Saved']=self.job_dir+"/"+strFILE_NAME+'_Saved'+"."+str(listBaseName[-1]).strip()
  
            strBaseName=os.path.basename(config['Paths']['training_data_path'])
            config['Paths']['training_data_path']=strJobDir+"/"+strBaseName

            strConfigPathOut=str(self.job_dir)+"/"+"config.ini"
            with open(strConfigPathOut, 'w') as configfile:
                config.write(configfile)
    
#    ************* Run Itemizer training *********************
            strErrorMessage,dictResults=self.runItemizer(strConfigPathOut,self.stage)     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if strErrorMessage!="":
#                 sys.exit(strErrorMessage)
                self.status = Status.ABORT
                return strErrorMessage            
            
            self.job.dict_predict_results=dictResults

            strDATASET_PATH_VER_SAVE=dictResults['predicted_file_path']
            strBaseName=os.path.basename(strDATASET_PATH_VER_SAVE)
            listBaseName=list(strBaseName.split('.'))
            strFILE_NAME_VER_SAVE=str(listBaseName[0]).strip()            
            strDataPathPklVerSave=os.path.dirname(strDATASET_PATH_VER_SAVE)+"/"+strFILE_NAME_VER_SAVE+'_DF.pkl'        
    
            myDataFrame = pd.read_pickle(strDataPathPklVerSave)
            listOfDFColumns = copy.copy(list(myDataFrame.columns))

            listTemp=copy.copy(list(myDataFrame.columns))          
            listOfDFColumns=["#"]
            listOfDFColumns.extend(listTemp)
    
            listOfLists=[]            
            for i in range(len(myDataFrame)):
                mlsRecord=myDataFrame.iloc[i,:].values
                listTemp2=mlsRecord.tolist()
                listTemp=[str(i+1)]
                listTemp.extend(listTemp2)            
                listOfLists.append(listTemp)

            self.job.extension_userdata={}            
            self.job.extension_userdata['train_data_list']=listOfLists       #    !!!!!!!!!!!!!!!!!!!!!!!!
            self.job.extension_userdata['train_columns_list']=listOfDFColumns       #    !!!!!!!!!!!!!!!!!!!!!!!!
            
            self.job.save()     #    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
        self.after_run()

        if self.status != Status.RUN:
            return "Status is not RUN."
        
        else:

            self.status = Status.DONE
            return strErrorMessage

    def abort(self):
        """
        Abort the Task
        """
        if self.status.is_running():
#             self.aborted.set()
            self.aborted=True       #    !!!!!!!!!!!!!!!!!!!!!!!

    def preprocess_output(self, line):
        """
        Takes line of output and parses it according to log format
        Returns (timestamp, level, message) or (None, None, None)
        """
        # NOTE: This must change when the logging format changes
        # YYYY-MM-DD HH:MM:SS [LEVEL] message
        match = re.match(r'(\S{10} \S{8}) \[(\w+)\s*\] (.*)$', line)
        if match:
            timestr = match.group(1)
            timestamp = time.mktime(time.strptime(timestr, log.DATE_FORMAT))
            level = match.group(2)
            message = match.group(3)
            if level.startswith('DEB'):
                level = 'debug'
            elif level.startswith('INF'):
                level = 'info'
            elif level.startswith('WAR'):
                level = 'warning'
            elif level.startswith('ERR'):
                level = 'error'
            elif level.startswith('CRI'):
                level = 'critical'
            return (timestamp, level, message)
        else:
            return (None, None, None)

    def process_output(self, line):
        """
        Process a line of output from the task
        Returns True if the output was able to be processed

        Arguments:
        line -- a line of output
        """
        raise NotImplementedError

    def est_done(self):
        """
        Returns the estimated time in seconds until the task is done
        """
        if self.status != Status.RUN or self.progress == 0:
            return None
        elapsed = time.time() - self.status_history[-1][1]
        return (1 - self.progress) * elapsed // self.progress

    def after_run(self):
        """
        Called after run() executes
        """
        pass

    def emit_progress_update(self):
        """
        Call socketio.emit for task progress update, and trigger job progress update.
        """
        from webapp import socketio
        socketio.emit('task update',
                      {
                          'task': self.html_id(),
                          'update': 'progress',
                          'percentage': int(round(100 * self.progress)),
                          'eta': utils.time_filters.print_time_diff(self.est_done()),
                      },
                      namespace='/jobs',
                      room=self.job_id,
                      )

        from webapp import scheduler
        job = scheduler.get_job(self.job_id)
        if job:
            job.emit_progress_update()
